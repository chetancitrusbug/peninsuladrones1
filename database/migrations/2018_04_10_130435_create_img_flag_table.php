<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImgFlagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagecounter', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('img_counter');
            $table->integer('blog_img_counter');
            $table->integer('blog_cat_id');
            $table->integer('portfolio_cat_id');
            $table->integer('banner_counter');
            $table->integer('freebanner_counter');
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('imagecounter');
    }
}
