<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('shortdesc')->nullable();
            $table->longText('description')->nullable();
            $table->string('image')->nullable();
            $table->integer('category_id');
            $table->integer('price');
            $table->integer('sale_price');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('product_image', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('products');
       Schema::dropIfExists('product_image');
    }
}
