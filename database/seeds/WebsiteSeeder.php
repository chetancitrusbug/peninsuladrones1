<?php

use Illuminate\Database\Seeder;
use App\WebSite;

class WebsiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        WebSite::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

//        \DB::table('_websites')->delete();


        $websites = [

            [

                'domain' => 'lara.gesmansys.com',
                'name' => 'Subdomain For Local testing.',
                'description' => 'only for local',
                'master' => true

            ],


            [

                'domain' => 'ibm.gesmansys.com',
                'name' => 'IBM',
                'description' => 'IBM COMPANY OF USA'
            ],
            [

                'domain' => 'microsoft.gesmansys.com',
                'name' => 'MicroSoft',
                'description' => 'Created by Bill Gates.'

            ],
            [

                'domain' => 'oracle.gesmansys.com',
                'name' => 'Oracle',
                'description' => 'Oracle is IT Company'

            ], [

                'domain' => 'gesmansys.dev',
                'name' => 'Main Domain For Testing.',
                'description' => 'only for local',
                'master' => true

            ], [

                'domain' => 'sub.gesmansys.dev',
                'name' => 'Subdomain For Local testing.',
                'description' => 'only for local'

            ],

            [
                'domain' => 'chronopost.gesmansys.com',
                'name' => 'Chronopost',
                'description' => 'Chronopost'
            ]


        ];


        foreach ($websites as $site) {
            WebSite::create($site);
        }


    }
}
