@extends('layouts.master')
@section('title','About us')
@section('content')
  <div class="page-header about padT100 padB100">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                        <!-- Theme Heading -->
                        <div class="breadcrumb-box">
                            <ul class="breadcrumb text-center colorW marB0">
                                <li>
                                    <a href="{{url('/')}}">Home</a>
                                </li>
                                <li class="active">About us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <!--//================About start==============//-->
        <div class="padT100 padB100">
            <div class="container">
                <div class="row">
                    <div class="about">
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <!-- Theme Heading -->
                            <div class="theme-heading hidden-xs">
                                <h1>about us</h1>
                                <h3><span class="heading-shape">About <strong>Peninsula Drone Photography</strong></span></h3>
                            </div>
                            <!-- Theme Heading -->
                            <div class="content">
                             
                               <p> Chris Lowe, the owner and operator of Peninsula Drone Photography has an extensive background in marketing and creative businesses beginning with ownership of Junior Casting and Vaude Vision talent agencies in 1980.</p>       

                               <p>Chris has worked extensively as a freelance photographer and established Freeze Frame studios in Queensland.  In 2006 he set up and operated a wordsmith studio, The Word Factory in suburban Melbourne.</p>

                               <p>Chris has extensive experience flying drones recreationally and recently qualified for Civil Aviation Safety Authority certification as a drone operator.  Peninsula Drone Photography currently operate two DJI drones, a Phantom 4 Professional and a larger Inspire 1 Professional with interchangeable standard and telephoto lenses.  These state of the art aircraft equip us for a broad range of creative and professional applications.</p>

                               <p>Chris has lived in bayside Melbourne most of his life and has been a resident of the Mornington Peninsula since the 1990’s.  Whilst Peninsula Drone Photography operates globally, our focus is providing our local Mornington Peninsula businesses and residents the unique creative and practical perspectives available through professional use of drone photography. </p>

                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--//================About end==============//-->
        
@endsection
