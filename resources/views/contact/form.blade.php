
	<div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 {{ $errors->has('name') ? ' has-error' : ''}}">           
                    {!! Form::text('name', null, ['placeholder' => 'Name']) !!} {!! $errors->first('name', '
                    <p class="help-block">:message</p>') !!}
        </div> 
        <div class="col-md-6 col-sm-6 col-xs-12 {{ $errors->has('email') ? ' has-error' : ''}}">           
                    {!! Form::email('email', null, ['placeholder' => 'Email']) !!} {!! $errors->first('email', '
                    <p class="help-block">:message</p>') !!}
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 {{ $errors->has('phone') ? ' has-error' : ''}}">           
                    <input type="text" class="form-control" value="" id="phone" name="phone" placeholder="Phone" onkeypress="return isNumber(event)" />
                     {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
        </div>
        
        <div class="col-md-6 col-sm-6 col-xs-12 {{ $errors->has('subject') ? ' has-error' : ''}}">           
                    {!! Form::text('subject', null, ['placeholder' => 'Subject']) !!} {!! $errors->first('subject', '
                    <p class="help-block">:message</p>') !!}
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 {{ $errors->has('question') ? ' has-error' : ''}}">           
                     {!! Form::textarea('question', null, ['placeholder' => 'Type your question','id' => 'question','size' => '30x5']) !!}
                     {!! $errors->first('question', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">           
                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'itg-button']) !!}
        </div>
    </div>    
     <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
            

    </script>        