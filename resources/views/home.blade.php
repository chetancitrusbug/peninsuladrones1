@extends('layouts.master')
@section('title','Home')
@section('content')
 <section id="slider-section">
            <div id="main-slider" class="owl-carousel owl-theme slider">
            @foreach($banners as $banner)
			 <?php                               
                         $banner_thumb_image = substr(strrchr($banner->image, '/'), 1);
                      ?>
			  @if(file_exists( public_path().'/banner/thumb/'.$banner_thumb_image ))
                <div class="item">
				
                    <figure class="slider-image">
                    
					
                        <img src="{{ asset('banner/thumb/'.$banner_thumb_image)}}" alt="{{$banner->title}}" />
                    </figure>
                    <div class="slider-text text-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h1>{{$banner->title}}</h1>
                                    <p>{{$banner->short_description}}</p>
									@if($banner->link)
										<a href="{{$banner->link}}" class="itg-button" target="_blank">More</a>
									@endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				@endif
                @endforeach                              
            </div>
        </section>
        <!--//================Slider end==============//-->
        <div class="clear"></div>
        <!--//================PENINSULA DRONE PHOTOGRAPHY start==============//-->
        <section class="padT100">
            <div class="container">
                <div class="row">
                    <div class="PENINSULA DRONE PHOTOGRAPHY">
                        <div class="col-md-4 col-sm-6 col-xs-12 pull-right ">
                            <!-- Theme Heading -->
                            <div class="theme-heading hidden-lg hidden-md hidden-sm">
                                <h1>PENINSULA DRONE PHOTOGRAPHY</h1>
                                <h3><span class="heading-shape">PENINSULA DRONE PHOTOGRAPHY</span></h3>
                            </div>
                            <!-- Theme Heading -->
                            <figure>
                                <img src="{{ asset('frontend/assets/img/all/PENINSULA DRONE PHOTOGRAPHY.jpg')}}" alt="" />
                            </figure>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12 pull-left ">
                            <!-- Theme Heading -->
                            <div class="theme-heading hidden-xs">
                                <h1>PENINSULA DRONE</h1>
                                <h3><span class="heading-shape">At home on the Mornington Peninsula - Spanning the Globe</span></h3>
                            </div>
                            <!-- Theme Heading -->
                            <div class="content">
                                <p>
                                    Peninsula Drone Photography is located on the picturesque Mornington Peninsula in Victoria, Australia and offers professional photography specialising in drone aerial photography and video.</p>
                                    <p>            
                                    Our aircraft are state of the art DJI photographic drones and produce high-resolution still photos and cinema quality 4K video for all applications including:</p>
                                    <ul class="home_content">
                                    <li>Real Estate – aerial images and videos artfully illustrate a property’s beauty and prime location</li>
                                    <li>Building, Construction and Asset Inspection Services – simple, efficient, cost effective and WHS effective</li>
                                    <li>Advertising and marketing– precision flying and skilled camera operation can send your campaign orbital</li>
                                    <li>Film, TV & Cinema productions– our DJI Inspire drone with X5 Zenmuse camera captures crystal clear cinematic video and stills through interchangeable lenses</li>
                                    <li>Corporate Events – drone photography is uniquely able to capture group shots, beautiful locations and activities at any conference or training event</li>
                                    <li>Special Events – contact us to discuss your event and we’ll work with you </li>
                                    </ul>
                                    <p>On the Mornington Peninsula?  Enquire about our special “Locals” rate!</p>

                                    <p> Peninsula Drone Photography is able to assist with any project in Australia or worldwide.  We’re happy to jump on a plane or have your job handled by a qualified professional affiliate.</p>

                                    <p>Need it quickly?  We have your projects completed within 48 hour of commencement!</p>

                                    <p>CASA licensed operator</p>

                                </p>
                               
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="my-story">
                                <!-- Theme Heading -->
                                <div class="theme-heading hidden-lg hidden-md hidden-sm">
                                    <h1>about us</h1>
                                    <h3><span class="heading-shape">about <strong>me</strong></span></h3>
                                </div>
                                <!-- Theme Heading -->
                                <a class="first"><img src="{{ asset('frontend/assets/img/all/Sunrise Shoreham Small.jpg')}}" alt="Inspire1 LGD"/></a>
                                <a class="second"><img src="{{ asset('frontend/assets/img/all/Inspire1 LGD SMALL.jpg')}}" alt="Sunrise Shoreham"/></a>
                                <a class="third active"><img src="{{ asset('frontend/assets/img/all/PHANTOM3 AIRBORNE SMALL.jpg')}}" alt="PHANTOM3 AIRBORNE"/></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================Services end==============//-->
        <div class="clear"></div>
        <!--//================Portfolio start==============//-->
        <section class="padT100 padB50">
            <div class="container">
                <div class="row">
                    <!-- Theme Heading -->
                    <div class="theme-heading">
                        <h1>our portfolio</h1>
                        <h3><span class="heading-shape">our <strong>portfolio</strong></span></h3>
                    </div>
                    <!-- Theme Heading -->
                </div>
            </div>
             <div class="container">
                <div class="row">
                    <div class="mixitup-btn col-xs-12 marB30">
                        <a class="filter"  data-filter="all"><span>ALL</span></a>
                        @foreach($portfoliocategory as $category)
                        <a class="filter"  data-filter=".{{$category->slug}}"><span>{{$category->title}}</span></a>
                        @endforeach
                    </div>
                </div>
            </div>
             
            <div class="mix-default container-fluid" >
                <div class="row">
                    <div id="mixItUp">
                     @foreach($portfoliolistbycat as $portfolios)  
                       
                        @foreach($portfolios as $portfoliosd)
                               <?php                               
								 $portfolio_thumb_image = substr(strrchr($portfoliosd->image, '/'), 1);
								 $portfolio_alt= str_replace("-"," ",$portfolio_thumb_image);
								 $ext = pathinfo($portfolio_alt, PATHINFO_EXTENSION);
								 $file = basename($portfolio_alt, ".".$ext); // $file is set to "index"
								  if($portfoliosd->title){
									   $portfolio_alt= $portfoliosd->title;
								  }else{
									   $portfolio_alt= $file;
								  }
							?>
							@if(file_exists( public_path().'/portfolio/thumb/'.$portfolio_thumb_image ))
								<div class="work-gallery col-md-3 col-sm-6 col-xs-12 mix  {{$portfoliosd->CategoryName->slug}}" >
									<div class="theme-hover">
										<figure>
											<?php                               
											 $portfolio_thumb_image = substr(strrchr($portfoliosd->image, '/'), 1);
										?>
											<img src="{{ asset('portfolio/thumb/'.$portfolio_thumb_image)}}" alt="{{$portfolio_alt}}"/>
											<figcaption>
												<div class="content">
													<div class="content-box">
														<a href="{{$portfoliosd->image}}" class="fancybox" data-fancybox-group="group"><i class="fa fa-file-image-o" aria-hidden="true"></i></a>
													</div>
												</div>
											</figcaption>
										</figure>
									</div>
								</div>
							@endif
                          @endforeach
                    @endforeach
                    </div>
                </div>
            </div>
           {{-- {{ $portfolio->links() }}--}}
        </section>
        <!--//================Portfolio end==============//-->
        <div class="clear"></div>
        <!--//===============Blog start==============//-->
        <section class="padT100 padB100">
            <div class="container">
                <div class="row">
                    <!-- Theme Heading -->
                    <div class="theme-heading">
                        <h1>our blog</h1>
                        <h3><span class="heading-shape">our <strong>Blog</strong></span></h3>
                    </div>
                    <!-- Theme Heading -->
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="blog-section">
                        @foreach($blog as $blogs)
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="blog">
                                <figure>
                                    @if(file_exists( public_path().'/Blogs/thumb/'.$blogs->image ) && $blogs->image !='')
                                        <a href="{!!url("blog/view/".$blogs->slug)!!}"><img src="{{asset ('Blogs/thumb/'.$blogs->image)}}" alt="{{$blogs->title}}" class="blog_list_img"/></a>
                                    @else
                                        <a href="{!!url("blog/view/".$blogs->slug)!!}"><img src="{{asset ('assets/images/default.jpg')}}" alt="{{$blogs->title}}" class="blog_list_img"/></a>
                                    @endif</a>
                                    <figcaption>
                                        <?php //$created_date= $blogs->created_at->format('F, j Y'); ?>
                                        <div class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $blogs->created_at->diffForHumans() }}</div>
                                    </figcaption>

                                   
                                </figure>
                                <div class="blog-caption">
                                    <h4><a href="{!!url("blog/view/".$blogs->slug)!!}">{{$blogs->title}}</a></h4>
                                    	<div class="blog-shortdesc">
                                            <p> {!! \Illuminate\Support\Str::words($blogs->shortdesc, 10) !!} </p>
                                        </div>
                                    <a href="{!!url("blog/view/".$blogs->slug)!!}" class="itg-button light">read more</a>
                                </div>
                            </div>
                        </div>
                        @endforeach                      
                    </div>
                </div>
            </div>
        </section>
        <!--//===============Blog end==============//-->
        <div class="clear"></div>
        <!--//================Contact start==============//-->
        <section>
            <div class="container">
                <!-- Theme Heading -->
                <div class="theme-heading">
                    <h1>Contact us</h1>
                    <h3><span class="heading-shape">Contact <strong>us</strong></span></h3>
                </div>
                <!-- Theme Heading -->
            </div>
			 @if (Session::has('flash_message'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if (Session::has('flash_error'))
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                            {{ Session::get('flash_error') }}
                        </div>
                    @endif
                    @if (Session::has('flash_success'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                    @include('flash::message')
            <div class="contact-us padT100 padB100">
                <div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="address">
								<h3>Contact</h3>
								<ul>
									<li><span>Address:</span>{{$setting->address}}</li>
									<li><span>Contact no:</span>{{$setting->contactno}}</li>
									<li><span>Email:</span>{{$setting->email}}</li>
								</ul>
							</div>
						</div>
						<div class="col-md-8 col-sm-6 col-xs-12">
							 {!! Form::open(['url' => '/contact', 'class' => 'theme-form','id'=>'formContact','enctype'=>'multipart/form-data']) !!}
                                @include ('contact.form')
                             {!! Form::close() !!}
						</div>
					</div>
                </div>
            </div>
        </section>
        <!--//================Contact end==============//-->
       <section>
            <div class="container  padT100 padB100">
                <!-- Theme Heading -->
                <div class="theme-heading">
                    <h1>Peninsula Drone Photography</h1>
                    <h3><span class="heading-shape">Peninsula Drone <strong>Photography</strong> SPANNING THE GLOBE</span></h3>
                </div>
                <!-- Theme Heading -->
            </div>
        </section> 
@endsection
