@extends('layouts.backend') 
@section('title','Contact Us') 
@section('pageTitle','Contact Us') 
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <i class="icon-circle-blank"></i> Contact Us
                </div>
            </div>
            <div class="box-content ">
                    <div class="row">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/contact', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                            {!! Form::close() !!}
                    </div>
                    <div class="table-responsive" >
                        <table class="table table-borderless" id="contact-table">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone No.</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
        </div>
    </div>
</div>
<script>
    $(function() { 
        var url ="{{ url('/admin/contact/') }}";
        var datatable = $('#contact-table').DataTable({
            "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route("contactData") !!}',
                    type: "get", // method , by default get
                },
                columns: [
                    { data: 'id', name: 'id',"searchable": false },
                    { data: 'name',name:'name',"searchable" : true}, 
                    { data: 'email',name:'email',"searchable" : true}, 
                    { data: 'phone',name:'phone',"searchable" : true},
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                                
                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> View</button></a>&nbsp;";   
                                                      
                            return v+e+d;
                        }
                    }
                    
                ]
        });
        
        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            
            var url ="{{ url('/admin/contact/') }}";
            url = url + "/" + id;
            var r = confirm("Are you sure you want to delete Category ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    success: function (data) {
                        datatable.draw();
                        toastr.success('Action Success!', data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procede!',erro)
                    }
                });
            }
        });
    }); 
</script>
@endsection
