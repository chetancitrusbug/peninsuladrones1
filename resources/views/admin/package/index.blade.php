@extends('layouts.backend')

@section('title','Packages')
@section('pageTitle','Packages')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                      Packages
                                                  </div>

                               </div>
                <div class="box-content ">


                    <div class="row">
                        <div class="col-md-6">
                                <a href="{{ url('/admin/packages/create') }}" class="btn btn-success btn-sm"
                                   title="Add New Document">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add New Package
                                </a>

                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/packages', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                            <input type="search" class="form-control search" name="search" placeholder="{{Request::get('search')}}" value="{!! request()->get('search') !!}">
                            
                            {!! Form::close() !!}
                            </div>
                        </div>
                    


                    <div class="table-responsive">
                        <table class="table table-borderless" id="products-table">
                            <thead>
                            <tr>
                                <th>Id</th>                         
                                <th>Title</th>
                                <th>Price</th>   
                                <th>Product</th>
                                <th>Actions</th>                        
                            </tr>
                            </thead>
                            <tbody>
                                    @foreach($packages as $item)
                                   
                                    <tr>
                                        <td> {{$item->id}}</td>
                                        <td> {{$item->title}}</td>                                       
                                        <td>{{$item->price}}</td>
                                        <td>@foreach($item->package_product as $product)
                                              {{$product->name}}
                                            @endforeach
                                        </td>
                                        <td>
        
                                                <a href="{{ url('/admin/packages/' . $item->id) }}" title="View package">
                                                    <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                           aria-hidden="true"></i> View
                                                    </button>
                                                </a>
        
                                                {{--  @if(Auth::user()->can('access.user.edit'))  --}}
        
                                                    <a href="{{ url('/admin/packages/' . $item->id . '/edit') }}"
                                                       title="Edit package">
                                                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                                  aria-hidden="true"></i> Edit
                                                        </button>
                                                    </a>
                                                {{--  @endif  --}}
        
                                                @if(Auth::user()->can('access.package.delete'))
        
                                                    {!! Form::open([
                                                        'method' => 'DELETE',
                                                        'url' => ['/admin/packages', $item->id],
                                                        'style' => 'display:inline'
                                                    ]) !!}
                                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-danger btn-xs',
                                                            'title' => 'Delete package',
                                                            'onclick'=>'return confirm("Confirm delete?")'
                                                    )) !!}
                                                    {!! Form::close() !!}
                                                @endif
        
                                            </td>
                                    </tr>   
                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pagination-wrapper"> {!! $packages->appends(['search' => Request::get('search')])->render() !!} </div>
                </div>
            </div>
        </div>
    </div>
@endsection

