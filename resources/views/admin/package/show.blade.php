@extends('layouts.backend')

@section('title','View Package')


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">View Package</div>
                <div class="panel-body">

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>
                    @if(Auth::user()->can('access.package.edit'))
                    <a href="{{ url('/admin/packages/' . $package->id . '/edit') }}" title="Edit Package">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            Edit Package
                        </button>
                    </a>
                    @endif
                    {!! Form::open([
                        'method' => 'DELETE',
                        'url' => ['/admin/packages', $package->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Package',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                            <tr>
                                <td>Id</td>
                                <td>{{ $package->id }}</td>
                            </tr>
                            <tr>
                                <td>Title</td>
                                <td>{{ $package->title }}</td>
                            </tr>

                            <tr>
                                <td>Price</td>
                                <td>{{ $package->price }}</td>
                            </tr>
                            @if( $package->kw)
                            <tr>
                                <td>KW</td>
                                <td>{{ $package->kw }}</td>
                            </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection