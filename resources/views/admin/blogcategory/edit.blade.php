@extends('layouts.backend')

@section('title','Edit Blog Category')
@section('pageTitle','Edit Blog Category')


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"> Edit Blog Category </div>
                <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($blogcategory, [
                        'method' => 'PATCH',
                        'url' => ['/admin/blogcategory', $blogcategory->id],
                        'class' => 'form-horizontal',
                        'id'=>'formBlogCategory',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.blogcategory.form', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
