@extends('layouts.backend') 
@section('title','View Blog') 
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Blog</div>
            <div class="panel-body">
                <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a> @if(Auth::user()->can('access.blog.edit'))
                <a href="{{ url('/admin/blogs/' . $blog->id . '/edit') }}" title="Edit Blog">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            Edit Blog
                        </button>
                    </a> @endif {!! Form::open([ 'method' => 'DELETE', 'url' => ['/admin/blogs', $blog->id],
                'style' => 'display:inline' ]) !!} {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete',
                array( 'type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'title' => 'Delete Blog', 'onclick'=>'return
                confirm("Confirm delete?")' ))!!} {!! Form::close() !!}
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            {{--  <tr>
                                <td>Id</td>
                                <td>{{ $blog->id }}</td>
                            </tr>  --}}

                            <tr>
                                <td>Name</td>
                                <td>{{ $blog->title }}</td>
                            </tr>
                            <tr>
                                <td> Description</td>
                                <td>{{ $blog->longdescription }}</td>
                            </tr>
                            @if($blog->shortdesc)
                            <tr>
                                <td>Short Description</td>
                                <td>{{ $blog->shortdesc }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td>Video Type</td>
                                <td>{{ $blog->video_type }}</td>
                            </tr>
                            <tr>
                                <td>Video Url</td>
                                <td>{{ $blog->video_url }}</td>
                            </tr>
                            <tr>
                                <td>Feature Image</td>
                                 @if(isset($blog->image))
                                <td><img src="{!! asset('Blogs/'.$blog->image) !!}" style="height:50px;width:50px;"></td>
                                @else
                                <td><img src="{{asset ('assets/images/default.jpg')}}" style="height:50px;width:50px;"></td>
                                @endif
                            </tr>
                            @if(isset($images) && $images)
                            <tr>
                                <td>Other Image</td>
                                <td>
                                    @foreach($images as $img_name)
                                    <img src="{!! asset('Blogs/'.$img_name->image) !!}" style="height:50px;width:50px;">                                    @endforeach
                                </td>
                            </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection