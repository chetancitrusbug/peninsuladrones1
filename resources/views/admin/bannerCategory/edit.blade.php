@extends('layouts.backend')

@section('title','Edit Portfolios')
@section('pageTitle','Edit Portfolios')


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"> Edit Portfolios </div>
                <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($bannerCategory, [
                        'method' => 'PATCH',
                        'url' => ['/admin/bannerCategory', $bannerCategory->id],
                        'class' => 'form-horizontal',
                        'id'=>'formCategory',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.bannerCategory.form', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
