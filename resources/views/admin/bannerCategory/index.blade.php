@extends('layouts.backend') 
@section('title','Portfolios') 
@section('pageTitle','Portfolios') 
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <i class="icon-circle-blank"></i> Portfolios
                </div>
            </div>
            <div class="box-content ">
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{ url('/admin/bannerCategory/create') }}" class="btn btn-success btn-sm" title="Add New Portfolios">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New Portfolios </a>
                        <a href="{{ url('/admin/portfolio') }}" class="btn btn-success btn-sm" title="Portfolio">
                        All Portfolios Images</a>
                    </div>
                    
                        <div class="col-md-6">
                            <select class="form-control navbar-form navbar-right selectStatusArea" name='status'>
                                <option value=''>All</option>
                                <option value='active'>Active</option>
                                <option value='inactive'>Inactive</option>
                            </select>
                        </div>
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/bannerCategory', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        {!! Form::close() !!}
                </div>
                <div class="table-responsive" >
                    <table class="table table-borderless" id="category-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function() { 
        var url ="{{ url('/admin/bannerCategory/') }}";
        var datatable = $('#category-table').DataTable({
            "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{!! route('bannerCategoryData') !!}',
                    type: "get", // method , by default get
                },
                columns: [
                    { data: 'id', name: 'id',"searchable": false },
                    { data: 'title',name:'bannercategory.title',"searchable" : true}, 
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status == 'inactive')
                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-success btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Inactive</button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> Active</button></a>";
                            
                            return status;
                                            
                        }

                    }, 
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                            var porturl ="{{ url('/admin/portfolio/create') }}";
                            var porturllist ="{{ url('/admin/bannerCategory/') }}";
                            var bulkporturl ="{{ url('/admin/bulkupload') }}";
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>Edit</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";
                                
                           var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> View</button></a>&nbsp;"; 
                           var r= "<a href='"+porturl+"?cat="+o.id+"'><button class='btn btn-success btn-xs'><i class='fa fa-file-image-o' aria-hidden='true'></i> Add Image</button></a>&nbsp;";
                           var s= "<a href='"+bulkporturl+"?cat="+o.id+"'><button class='btn btn-success btn-xs'><i class='fa fa-file-image-o' aria-hidden='true'></i> Add Bulk Portfolios</button></a>&nbsp;";
                           var t= "<a href='"+porturllist+"/"+o.id+"/portfoliocatlist'><button class='btn btn-success btn-xs'><i class='fa fa-file-image-o' aria-hidden='true'></i> Portfolios listing</button></a>";
                                                      
                            return v+e+d+r+s+t;
                        }
                    }
                    
                ]
        });
        $( ".selectStatusArea" ).change(function() {  
            var status = $( ".selectStatusArea" ).val();
            var url ="{!! route('bannerCategoryData') !!}";
            datatable.ajax.url( url + '?status='+ status).load();            
        });
        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            
            var url ="{{ url('/admin/bannerCategory/') }}";
            url = url + "/" + id;
            var r = confirm("Are you sure you want to delete Portfolios ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    success: function (data) {
                        datatable.draw();
                        toastr.success('Action Success!', data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procede!',erro)
                    }
                });
            }
        });
    }); 
</script>
@endsection
