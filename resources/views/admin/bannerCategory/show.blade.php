@extends('layouts.backend') 
@section('title','View Portfolios') 
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Portfolios</div>
            <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a> 
                    <a href="{{ url('/admin/bannerCategory/' . $bannerCategory->id . '/edit') }}" title="Edit Category">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            Edit Portfolios
                        </button>
                    </a>
                <br/>
                <br/>

                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <td>Id</td>
                                <td>{{ $bannerCategory->id }}</td>
                            </tr>
                            <tr>
                                <td>Title</td>
                                <td>{{ $bannerCategory->title }}</td>
                            </tr>
                            <tr>
                                <td> Status</td>
                                <td>{{ $bannerCategory->status }}</td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection