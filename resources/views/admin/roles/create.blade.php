@extends('layouts.backend')


@section('title','Create Role')


@section('content')
        <div class="row">

            <div class="col-md-12">
                <div class="box bordered-box blue-border">
                        <div class="box-header blue-background">
                            <div class="title">
                                <i class="icon-circle-blank"></i>
                                Create Role
                            </div>
                            {{--  <div class="actions">
                                @include('partials.page_tooltip',['model' => 'role','page'=>'form'])
                            </div>  --}}

                        </div>
                        <div class="box-content ">

                        <a href="{{ url('/admin/roles') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

                        
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/roles', 'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off']) !!}

                        @include ('admin.roles.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
    </div>
@endsection