<div class="row">
   
   <!-- <div class="col-sm-12 floatLeft">
            <div class="col-sm-6">   
                <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                    {!! Form::label('categoryId', 'Category', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::select('categoryId',$bannerCategory,null, ['class' => 'form-control']) !!}
                        {!! $errors->first('categoryId','<p class="help-block with-errors">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>-->
         {!! Form::hidden('categoryId', $categoryId, ['class' => 'form-control']) !!}

        <div class="col-sm-12 floatLeft">
            <div class="col-sm-6">   
                <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                    {!! Form::label('image', 'Bulk Images', ['class' => 'col-sm-4 control-label']) !!}                    
                </div>
                <div class="module_ad">
                    <div class="module__body">
                        <fieldset>
                            <div class="form-row">
                                <div class="fileupload-container">
                                        <ul id="loaded-files" class="upload-image-thumbs">
                                            <li class="upload-placeholder" style="width: auto;">
                                        
                                                <input type="file" name="image[]" id="image" class="images" max-uploads = "4"
                                                accept="image/*" multiple style="display:none">

                                             </li>
                                         </ul>
                                </div>
                                <div class="has-error activity_image_error" style="display: none">
                                    <p class="help-block">Maximum image limit exceeded , You can upload maximum 4 Images</p>
                                </div>

                             </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            
        </div>
        
       
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">  
            <div class="form-group">
                <div class="col-sm-offset-8 col-sm-4">
                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
        </div>
    </div> 
</div>     
              @push('js')
<script>

    $(document).ready(function () {
        $('input[type="file"]').imageuploadify();
    });

</script>
@endpush