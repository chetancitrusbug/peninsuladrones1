@extends('layouts.backend')

@section('title','View Product')


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Product</div>
                <div class="panel-body">

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>
                    @if(Auth::user()->can('access.product.edit'))
                    <a href="{{ url('/admin/products/' . $product->id . '/edit') }}" title="Edit Product">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            Edit Product
                        </button>
                    </a>
                    @endif
                    {!! Form::open([
                        'method' => 'DELETE',
                        'url' => ['/admin/products', $product->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Product',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>Id</td>
                                <td>{{ $product->id }}</td>
                            </tr>


                            <tr>
                                <td>Name</td>
                                <td>{{ $product->name }}</td>
                            </tr>
                       
                            <tr>
                                <td>Description</td>
                                <td>{{ $product->description }}</td>
                            </tr>
                            @if($product->shortdesc)
                            <tr>
                                <td>Short Description</td>
                                <td>{{ $product->shortdesc }}</td>
                            </tr>
                            @endif 
                            <tr>
                                <td>Price</td>
                                <td>{{ $product->price }}</td>
                            </tr> 
                            <tr>
                                <td>Sale Price</td>
                                <td>{{ $product->sale_price }}</td>
                            </tr>  
                            <tr>
                                <td>Category</td>
                                <td>{{ $product->cat_name }}</td>
                            </tr> 
                            <tr>
                                <td>Feature Image</td>
                                <td><img src="{!! asset('Products/'.$product->image) !!}" style="height:50px;width:50px;"></td>
                            </tr> 
                            @if(isset($images) && $images)
                            <tr>
                                    <td>Other Image</td>
@foreach($images as $img_name)

        <td><img src="{!! asset('Products/'.$img_name->image) !!}" style="height:50px;width:50px;"></td>
     
@endforeach
</tr>
@endif


                                            
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection