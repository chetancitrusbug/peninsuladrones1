<div class="row">
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
                {!! Form::label('email', 'Email *', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::email('email', null, ['class' => 'form-control']) !!} {!! $errors->first('email', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div> 
    </div>
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->has('twitter') ? ' has-error' : ''}}">
                {!! Form::label('twitter', 'Twitter Url', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                       {!! Form::text('twitter', null, ['class' => 'form-control']) !!} 
                        {!! $errors->first('twitter', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div> 
    </div>
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->has('facebook') ? ' has-error' : ''}}">
                {!! Form::label('facebook', 'Facebook Url ', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('facebook', null, ['class' => 'form-control']) !!} 
                    {!! $errors->first('facebook', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div> 
    </div>

    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->has('pintrest') ? ' has-error' : ''}}">
                {!! Form::label('pintrest', 'Pinterest Url ', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('pintrest', null, ['class' => 'form-control']) !!} 
                    {!! $errors->first('pintrest', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div> 
    </div>

     <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->has('insta') ? ' has-error' : ''}}">
                {!! Form::label('insta', 'Instagram Url ', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('insta', null, ['class' => 'form-control']) !!} 
                    {!! $errors->first('insta', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div> 
    </div>

    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->has('address') ? ' has-error' : ''}}">
                {!! Form::label('address', 'Address ', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('address', null, ['class' => 'form-control']) !!} 
                    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div> 
    </div>
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->has('contactno') ? ' has-error' : ''}}">
                {!! Form::label('contactno', 'Contact No ', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('contactno', null, ['class' => 'form-control']) !!} 
                    {!! $errors->first('contactno', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div> 
    </div>

    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">  
            <div class="form-group">
                <div class="col-sm-offset-8 col-sm-4">
                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
        </div>
    </div> 
</div>                   