<div class="row">
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
                {!! Form::label('title', 'Title *', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('title', null, ['class' => 'form-control']) !!} {!! $errors->first('title', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div> 
    </div>
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->has('short_description') ? ' has-error' : ''}}">
                {!! Form::label('short_description', 'Short Description *', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                        {!! Form::textarea('short_description', null, ['class' => 'form-control','id' => 'description','size' => '30x5']) !!}
                        {!! $errors->first('short_description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div> 
    </div>
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->has('link') ? ' has-error' : ''}}">
                {!! Form::label('link', 'Link', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('link', null, ['class' => 'form-control']) !!} 
                    {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div> 
    </div>

    @if(isset($banner) && $banner->image)
        
        <div class="col-sm-12 floatLeft">
            <div class="col-sm-6">
                {!! Form::label('image',  'View Image', ['class' => 'col-md-4 control-label']) !!}
                <img src="{!! $banner->image !!}" alt="Banner" width="100px" class="viewImage">
                <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                    {!! Form::label('image',  'change image', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        <input type="file" name="image" id="image" >
                        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="col-sm-12 floatLeft">
            <div class="col-sm-6">
                <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                    {!! Form::label('image', 'Image *', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        <input type="file" name="image" id="image" >
                        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>   
    @endif

@if(Route::currentRouteName() == 'banner.edit')
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">   
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('status',['active'=>'Active','inactive'=>'Inactive'] ,null, ['class' => 'form-control']) !!} {!! $errors->first('status','<p class="help-block with-errors">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
@endif
<div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->has('sort_no') ? ' has-error' : ''}}">
                {!! Form::label('sort_no', 'Sort No *', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('sort_no', null, ['class' => 'form-control']) !!} 
                    {!! $errors->first('sort_no', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div> 
    </div>
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">  
            <div class="form-group">
                <div class="col-sm-offset-8 col-sm-4">
                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
        </div>
    </div> 
</div>                   