<!DOCTYPE html>
<html>
	<head>
		<title>Peninsula Drones</title>
		<style>
			h2 {
				color:#BF1E2E;
				font-size: 20px;
				text-align: center;
			}
			table {
				border-collapse: collapse;
				width: 50%;
				text-align: center;
				margin:0px auto 20px auto;
        
			}

			th, td {
				text-align: left;
				padding: 8px;
				width: 50%;
				font-size: 15px;
			}

			tr:nth-child(odd){background-color: #f2f2f2}

			th {
				background-color: #4CAF50;
				color: white;
			}
			
			p {
				margin:30px; 
				font-size:15px;
			}
		
		</style>
	</head>
	<body style="margin:30px auto; width:700px;">
		
		<div style="align:center; border:1px solid #ccc; padding-top:30px; padding-bottom:40px">
			<h2>Contact Form Inquiry</h2>
			<p>hello admin,</p>	
			<table>	
	
				<tr>
					<td>Name:</td>
					<td>{{$contact->name}}</a></td>					
				</tr>
				<tr>
					<td>Email:</td>
					<td>{{$contact->email}}</a></td>					
				</tr>
				<tr>
					<td>Phone:</td>
					<td>{{$contact->phone}}</a></td>					
				</tr>
				<tr>
					<td>Subject:</td>
					<td>{{$contact->subject}}</a></td>					
				</tr>
				<tr>
					<td>Question:</td>
					<td>{{$contact->question}}</a></td>					
				</tr>
			</table>
		</div>
		
	</body>
</html>