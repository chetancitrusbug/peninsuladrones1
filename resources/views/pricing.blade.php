@extends('layouts.master')
@section('title','Pricing')
@section('content')
<!--//================Header end==============//--> 
        <div class="page-header about padT100 padB100">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Theme Heading -->
                        <div class="theme-heading">
                            <!--<h1>Pricing Table</h1>-->
                            <h3><span class="heading-shape">Pricing <strong>Table</strong></span></h3>
                        </div>
                        <!-- Theme Heading -->
                        <div class="breadcrumb-box">
                            <ul class="breadcrumb text-center colorW marB0">
                                <li>
                                    <a href="{{url('/')}}">Home</a>
                                </li>
                                <li class="active">Pricing Table </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <!--//================About start==============//-->
        
        <!--//================About end==============//-->
        <!--//================Photography start==============//-->
       
        <!--//================Photography end==============//-->
        <div class="padT100 padB100">
            <div class="container">
                <div class="row">
                    <div class="photographer">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                
                                <section class="pricing-table-section">
                                    <div class="pricing-table-div">
                                        <div class="top-row-div">
                                            <h3>Aerial Photography</h3>
                                            <p>Real estate photo session</p>
                                        </div><!-- end of top-row-div -->
                                        
                                        <div class="second-row-div">
                                            <h2><span>$</span><span>198.00</span></h2>
                                            <p>Single Location</p>
                                        </div><!-- end of second row div -->

                                        <div class="middle-row-div">
                                            
                                            <div class="row-line">
                                                <div class="div-left-row pull-left">
                                                    <i class="fas fa-camera"></i>
                                                </div>
                                                <div class="div-right-row">
                                                    <p>Choose 5 From 20 Shots</p>
                                                </div>
                                            </div><!-- row-line -->

                                            <div class="row-line">
                                                <div class="div-left-row pull-left">
                                                    <i class="fas fa-wrench"></i>
                                                </div>
                                                <div class="div-right-row">
                                                    <p>In-hours Photo Correction</p>
                                                </div>
                                            </div><!-- row-line -->
                                            
                                            <div class="row-line">
                                                <div class="div-left-row pull-left">
                                                    <i class="far fa-clock"></i>
                                                </div>
                                                <div class="div-right-row">
                                                    <p>1 Day Turn-Around</p>
                                                </div>
                                            </div><!-- row-line -->

                                        </div><!-- end of middle-row-div -->

                                        <div class="footer-row-div">

                                            <div class="footer-1">
                                                <span>
                                                    Please contact us ahead of time to schedule a photo session 
                                                    and to discuss the details of the photo shoots.
                                                </span>
                                            </div>

                                            <div class="footer-btn-div">
                                                <a href="{{url('/contact')}}" class="btn-footer"><i class="fas fa-shopping-cart"></i> Contact us to purchase Shoot</a>
                                            </div>
                                        </div>

                                    </div><!-- end of princing table div -->
                                </section><!-- end of princing table section -->
                                
                            </div><!-- end of col-1 -->

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                
                                    <section class="pricing-table-section">
                                        <div class="pricing-table-div">
                                            <div class="top-row-div">
                                                <h3>Aerial Video</h3>
                                                <p>Real estate video session</p>
                                            </div><!-- end of top-row-div -->
                                            
                                            <div class="second-row-div">
                                                <h2><span>$</span><span>198.00</span></h2>
                                                <p>Single Location</p>
                                            </div><!-- end of second row div -->
    
                                            <div class="middle-row-div">
                                                
                                                <div class="row-line">
                                                    <div class="div-left-row pull-left">
                                                        <i class="fas fa-video"></i>
                                                    </div>
                                                    <div class="div-right-row">
                                                        <p>1-2 Minutes Aerial Video</p>
                                                    </div>
                                                </div><!-- row-line -->
    
                                                <div class="row-line">
                                                    <div class="div-left-row pull-left">
                                                        <i class="fas fa-wrench"></i>
                                                    </div>
                                                    <div class="div-right-row">
                                                        <p>Fully Edited video W/ Music</p>
                                                    </div>
                                                </div><!-- row-line -->
                                                
                                                <div class="row-line">
                                                    <div class="div-left-row pull-left">
                                                        <i class="far fa-clock"></i>
                                                    </div>
                                                    <div class="div-right-row">
                                                        <p>2 Day Turn-Around</p>
                                                    </div>
                                                </div><!-- row-line -->
    
                                            </div><!-- end of middle-row-div -->
    
                                            <div class="footer-row-div">
    
                                                <div class="footer-1">
                                                    <span>
                                                        Please contact us ahead of time to schedule a video session 
                                                        and to discuss the details of the video shoots.
                                                    </span>
                                                </div>
    
                                                <div class="footer-btn-div">
                                                    <a href="{{url('/contact')}}" class="btn-footer"><i class="fas fa-shopping-cart"></i> Contact us to purchase Shoot</a>
                                                </div>
                                            </div>
    
                                        </div><!-- end of princing table div -->
                                    </section><!-- end of princing table section -->
                                    
                                </div><!-- end of col-2 -->

                                <div class="col-md-4 col-sm-6 col-xs-12">
                                
                                        <section class="pricing-table-section">
                                            <div class="pricing-table-div">
                                                <div class="top-row-div">
                                                    <h3>Aerial Photo/video</h3>
                                                    <p>Real estate Photo/video session</p>
                                                </div><!-- end of top-row-div -->
                                                
                                                <div class="second-row-div">
                                                    <h2><span>$</span><span>353.00</span></h2>
                                                    <p>Single Location</p>
                                                </div><!-- end of second row div -->
        
                                                <div class="middle-row-div">
                                                    
                                                    <div class="row-line">
                                                        <div class="div-left-row pull-left">
                                                            <i class="fas fa-camera"></i>
                                                        </div>
                                                        <div class="div-right-row">
                                                            <p>Choose 5 From 20 Shots</p>
                                                        </div>
                                                    </div><!-- row-line -->
        
                                                    <div class="row-line">
                                                        <div class="div-left-row pull-left">
                                                            <i class="fas fa-video"></i>
                                                        </div>
                                                        <div class="div-right-row">
                                                            <p>Fully Edited Video W/ Music</p>
                                                        </div>
                                                    </div><!-- row-line -->
                                                    
                                                    <div class="row-line">
                                                        <div class="div-left-row pull-left">
                                                            <i class="far fa-clock"></i>
                                                        </div>
                                                        <div class="div-right-row">
                                                            <p>1 Day Turn-Around</p>
                                                        </div>
                                                    </div><!-- row-line -->
        
                                                </div><!-- end of middle-row-div -->
        
                                                <div class="footer-row-div">
        
                                                    <div class="footer-1">
                                                        <span>
                                                            Both aerial photo and video sessions must be used at the same location for this offer.
                                                        </span>
                                                    </div>
        
                                                    <div class="footer-btn-div">
                                                        <a href="{{url('/contact')}}" class="btn-footer"><i class="fas fa-shopping-cart"></i> Contact us to purchase Shoot</a>
                                                    </div>
                                                </div>
        
                                            </div><!-- end of princing table div -->
                                        </section><!-- end of princing table section -->
                                        
                                    </div><!-- end of col-3 -->

                                    <div class="col-md-6 col-sm-6 col-xs-12 style-footer">

                                        <section class="footer-details">

                                            <p>For Local* & Multiple locations dicounts please email enquiry@peninsuladrones.com.au</p>

                                            <p class="star pull-left">*</p><p>Applies to residents & businesses situated on the Mornington Peninsula.</p>

                                            <p class="bold">Please Note:</p>

                                            <ol class="ol">
                                                <li>
                                                   1. Pricing applies to jobs done solely by Peninsula Drones Photography without the engagement of affiliates or sub-contractors.
                                                </li>
                                                <li>
                                                   2. Travel charges may apply to locations in excess of 100 Km from Hastings, Victoria.
                                                </li>
                                            </ol>

                                        </section>

                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <img src="{{asset ('assets/images/Local Map.jpg')}}" alt="Map">
                                    </div>
                                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        
        
@endsection