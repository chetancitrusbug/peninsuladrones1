@extends('layouts.master')

@section('title','Blogs')

@section('content')
 <div class="page-header blog-one padT100 padB70">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Theme Heading -->
                        <div class="theme-heading">
                            <h1>our blog</h1>
                            <h3><span class="heading-shape">our <strong>blog</strong></span></h3>
                        </div>
                        <!-- Theme Heading -->
                        <div class="breadcrumb-box">
                            <ul class="breadcrumb text-center colorW marB0">
                                <li>
                                    <a href="{{url('/')}}">Home</a>
                                </li>
                                <li class="active">Blog</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
 <!--//================Portfolio start==============//-->
        <section class="padT100 padB70">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-7 col-xs-12">
                        <div class="row">
                            <div class="blog-section with-sidebar">
                            @foreach($blogs as $blog)
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="blog marB30">
                                        <figure>
                                          	@if(file_exists( public_path().'/Blogs/thumb/'.$blog->image ) && $blog->image !='')
                                                <a href="{!!url("blog/view/".$blog->slug)!!}"><img src="{{asset ('Blogs/thumb/'.$blog->image)}}" alt="{{$blog->title}}" class="blog_list_img"/></a>
                                            @else
                                                <a href="{!!url("blog/view/".$blog->slug)!!}"><img src="{{asset ('assets/images/default.jpg')}}" alt="{{$blog->title}}" class="blog_list_img"/></a>
                                            @endif
                                            <figcaption>
                                                <div class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $blog->created_at->diffForHumans() }}</div>
                                            </figcaption>
                                        </figure>
                                        <div class="blog-caption">
                                            <h4><a href="{!!url("blog/view/".$blog->slug)!!}">{{$blog->title}}</a></h4>
                                           <div class="blog-shortdesc">
                                                 {!! \Illuminate\Support\Str::words($blog->shortdesc, 10) !!} 
                                                 </div>
                                                   {{-- {!! str_limit($blog->shortdesc,20) !!} --}}
                                               
                                            <a href="{!!url("blog/view/".$blog->slug)!!}" class="itg-button light">read more</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                                
                               
                                <div class="col-xs-12">
                                    <div class="pagination">
                                        <ul class="pull-left">
                                            <li>{!! $blogs->links() !!}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-5 col-xs-12">
                        <div class="row">
                            <div class="blog-sidebar">
                                <div class="col-xs-12 widget">
                                    <h3 class="widget_title">Categories</h3>
                                    <ul class="sidebar-category">
                                        @foreach($blogcategory as $catgoryname)
                                            <li><a href="{!!url("blog/category/".$catgoryname->slug)!!}">{{$catgoryname->title}}<span>({{count($catgoryname->blog)}})</span></a></li>
                                        @endforeach
                                    </ul>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

@endsection
