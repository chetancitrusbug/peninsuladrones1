@extends('layouts.master')

@section('title',$blog->page_title)
@section('headerMeta')
<meta property="og:title" content="{{$blog->title}}"/>
<meta property="og:description" content="{{$blog->shortdesc}}"/>
<meta name="description" content="{{$blog->shortdesc}}"/>
<meta name="author" content="itgeeksin.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">  
 <title>{{$blog->title}}</title>     
@endsection

@section('content')

<div class="page-header blog-full-side padT100 padB70">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Theme Heading -->
                        <div class="theme-heading">
                            <h1>our blog</h1>
                            <h3><span class="heading-shape">our <strong>blog</strong></span></h3>
                        </div>
                        <!-- Theme Heading -->
                        <div class="breadcrumb-box">
                            <ul class="breadcrumb text-center colorW marB0">
                                <li>
                                    <a href="{{url('/')}}">Home</a>
                                </li>
                                <li class="active">Blog</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <!--//================Portfolio start==============//-->
        <section class="padT100 padB100">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-7 col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="blog-article">
                                    <figure>
                                        @if(file_exists( public_path().'/Blogs/thumb/'.$blog->image ) && $blog->image !='')
											<a href="{!!url("blog/view/".$blog->slug)!!}"><img src="{{asset ('Blogs/thumb/'.$blog->image)}}" alt="{{$blog->title}}" class="blog_list_img"/></a>
										@else
											<a href="{!!url("blog/view/".$blog->slug)!!}"><img src="{{asset ('assets/images/default.jpg')}}" alt="{{$blog->title}}" class="blog_list_img"/></a>
										@endif
                                    </figure>
                                    <h3>{{$blog->title}} ( {{ $blog->categoryName->cat_title }} )</h3>
                                    <div class="blog-info">
                                         <?php //$created_date= $blog->created_at->format('F, j Y'); ?>
                                        <i class="fa fa-clock-o" aria-hidden="true"></i> {{ $blog->created_at->diffForHumans() }}
                                    </div>
                                    <p> {!!$blog->shortdesc!!} </p>
                                   <p> {!!$blog->longdescription!!}</p>
                                    </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="language-gallery blog_video_gallery">
                                    <h3>Blog Gallery </h3>
                                    <div class="row">
                                    @if(count($bloggallery))
                                        @foreach($bloggallery as $gallery)
											@if(file_exists( public_path().'/Blogs/thumb/'.$gallery->image ))
												
												<div class="work-gallery simple  col-md-6 col-sm-6 col-xs-12">
													<div class="theme-hover blog_gallery">
														<figure>
															<img src="{{ asset('Blogs/thumb/'.$gallery->image)}}" alt="{{$gallery->blog->title}}"/>
															<figcaption>
																<div class="content">
																	<div class="content-box">
																		<a href="{{ asset('Blogs/'.$gallery->image)}}" class="fancybox" data-fancybox-group="group"><i class="fa fa-file-image-o" aria-hidden="true"></i></a>
																	</div>
																</div>
															</figcaption>
														</figure>
													</div>
												</div>
												
											@endif
                                        @endforeach 
                                      @else
                                        <div class="work-gallery simple  col-md-6 col-sm-12 col-xs-12">
                                            No More Images
                                        </div>
                                    @endif                      
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-xs-12">
							@if($blog->video_url)
								@if($blog->video_type =='youtube')
								<iframe width="640" height="360" src="{{$blog->video_url}}" frameborder="0" allowfullscreen></iframe>
								@else
								 <iframe src="{{$blog->video_url}}" width="640" height="360" frameborder="0"></iframe>
								@endif
                            @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-5 col-xs-12">
                        <div class="row">
                            <div class="blog-sidebar">
                                
                               <div class="col-xs-12 widget">
                                    <h3 class="widget_title">Categories</h3>
                                    <ul class="sidebar-category">
                                        @foreach($blogcategory as $catgoryname)
                                        <li><a href="{!!url("blog/category/".$catgoryname->slug)!!}">{{$catgoryname->title}}<span>({{count($catgoryname->blog)}})</span></a></li>
                                        @endforeach
                                    </ul>
                                </div>
                                 @include('partials.resentPost')
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--//================Portfolio end==============//-->
@endsection
