@extends('layouts.master')

@section('title',$portfoliocategory->title)

@section('content')

<!--//================Header end==============//--> 
        <div class="page-header portfolio-four padT100 padB70">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Theme Heading -->
                        <div class="theme-heading">
                            <h1>our portfolio</h1>
                            <h3><span class="heading-shape"><strong>{{$portfoliocategory->title}}</strong></span></h3>
                        </div>
                        <!-- Theme Heading -->
                        <div class="breadcrumb-box">
                            <ul class="breadcrumb text-center colorW marB0">
                                <li>
                                    <a href="{{url('/')}}">Home</a>
                                </li>
                                <li>Portfolio</li>
                                <li class="active">{{$portfoliocategory->title}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <!--//================Portfolio start==============//-->
        <div class="padT100 padB100">          
            <div class="mix-default container" >
                <div class="row">
                    <div id="mixItUp">
                      @if(count($portfolio))
                        @foreach($portfolio as $portfolios)
							<?php                               
								 $portfolio_thumb_image = substr(strrchr($portfolios->image, '/'), 1);
								 $portfolio_alt= str_replace("-"," ",$portfolio_thumb_image);
								 $ext = pathinfo($portfolio_alt, PATHINFO_EXTENSION);
								 $file = basename($portfolio_alt, ".".$ext); // $file is set to "index"
								  if($portfolios->title){
									   $portfolio_alt= $portfolios->title;
								  }else{
									   $portfolio_alt= $file;
								  }
							?>
							@if(file_exists( public_path().'/portfolio/thumb/'.$portfolio_thumb_image ))
								<div class="work-gallery simple  col-md-4 col-sm-6 col-xs-12 mix {{$portfoliocategory->title}}">
									<div class="theme-hover portfolio_gallery">
										<figure>
									   
										   <img src="{{ asset('portfolio/thumb/'.$portfolio_thumb_image)}}" alt="{{$portfolio_alt}}"/>
											<figcaption>
												<div class="content">
													<div class="content-box">
														<a href="{{$portfolios->image}}" class="fancybox" data-fancybox-group="group"><i class="fa fa-file-image-o" aria-hidden="true"></i></a>
													</div>
												</div>
											</figcaption>
										
										</figure>
									</div>
								</div>
							@endif
                        @endforeach
                    @else
                   <p class="no_category">No Portfolio Found</p>
                    @endif
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="pagination">
                    <ul>
                        <li>{!! $portfolio->links() !!}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!--//================Portfolio end==============//-->
@endsection