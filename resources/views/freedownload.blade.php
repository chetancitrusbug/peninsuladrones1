@extends('layouts.master')

@section('title','Free Download Images')

@section('content')

<!--//================Header end==============//--> 
        <div class="page-header portfolio-four padT100 padB70">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Theme Heading -->
                        <div class="theme-heading">
                            
                        </div>
                        <!-- Theme Heading -->
                        <div class="breadcrumb-box">
                            <ul class="breadcrumb text-center colorW marB0">
                                <li>
                                    <a href="{{url('/')}}">Home</a>
                                </li>
                                <li>Freedownload</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <!--//================Portfolio start==============//-->
        <div class="padT100 padB100">          
            <div class="mix-default container" >
                <div class="row">
                    <div id="mixItUp">
                      @if(count($freebanner))
                        @foreach($freebanner as $freebanners)
                         <div class="work-gallery simple  col-md-4 col-sm-4 col-xs-12 mix">
                            <div class="theme-hover download_gallery">
                                <figure>
                               
                                   <img src="{{ asset('freebanner/thumb/'.$freebanners->image)}}" alt="{{$freebanners->title}}"/>
                                    <figcaption>
                                        <div class="content">
                                            <div class="content-box">
                                                <a href="#" id="freedownload" data-banners="{{$freebanners->id}}"><i class="fa fa-file-image-o" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        @endforeach
                    @else
                   <p class="no_category">No Images Found</p>
                    @endif
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="pagination">
                    <ul>
                        <li>{!! $freebanner->links() !!}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!--//================Portfolio end==============//-->
@endsection

