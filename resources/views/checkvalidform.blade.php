@extends('layouts.master')
@section('title','Free Download Image')
@section('content')
  <div class="page-header about padT100 padB100">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                        <!-- Theme Heading -->
                        <div class="breadcrumb-box">
                            <ul class="breadcrumb text-center colorW marB0">
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <!--//================About start==============//-->
        <div class="padT100 padB100">
            <div class="container">
                <div class="row">
                    <div class="about">
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <!-- Theme Heading -->
                            <div class="theme-heading hidden-xs">                              
                                <h2 class="message_error">@if(isset($message)){{$message}}@endif</h2>
                            </div>
                            <!-- Theme Heading -->
                            <div class="content">
                             @if(isset($userlog))
                               <img src="{{$userlog}}">
							@endif
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--//================About end==============//-->
        
@endsection
