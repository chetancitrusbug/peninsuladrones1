<?php


Auth::routes();

    Route::get('/', 'HomeController@index');
    Route::get('/about', 'HomeController@aboutindex');
    Route::get('/pricing', 'HomeController@pricingindex');
    Route::get('/free-download', 'FreeimagedownloaduserlogController@index');
    Route::get('/free-download/checkvalid', 'FreeimagedownloaduserlogController@checkvalid');
    Route::get('/freedownloaduser', 'FreeimagedownloaduserlogController@add');
    Route::post('/freedownloaduser', 'FreeimagedownloaduserlogController@add');
    Route::group(['prefix' => 'blog'], function() {
        // blog    
        Route::get('/', 'BlogController@index');
        Route::get('/view/{slug}', 'BlogController@view');
        Route::get('/category/{category}', 'BlogController@serchbycategory');
    });
    Route::group(['prefix' => 'portfolio'], function() {
        //Portfolio
        Route::get('/view/{slug}', 'PortfolioController@view');
    });

        //Contact
        Route::resource('/contact', 'ContactController');


    Route::group(['middleware' => ['auth', 'admin']], function () {
    
    Route::get('/admin', 'HomeController@redirect');

    Route::group(['prefix' => 'admin'], function () {

        Route::get('/admin', 'Admin\AdminController@index');
        Route::resource('/users', 'Admin\UsersController');
        //     Route::get('/give-role-permissions', 'Admin\AdminController@getGiveRolePermissions');
        //     Route::post('/give-role-permissions', 'Admin\AdminController@postGiveRolePermissions');
        
         Route::get('/profile', 'Admin\ProfileController@index')->name('profile.index');
       Route::get('/profile/edit', 'Admin\ProfileController@edit')->name('profile.edit');
         Route::patch('/profile/edit', 'Admin\ProfileController@update');

         Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');
         Route::patch('/profile/change-password', 'Admin\ProfileController@updatePassword');

    //     Route::resource('/category', 'Admin\CategoryController');
    //     Route::resource('/products', 'Admin\ProductController');
    //     Route::get('/products/create', 'Admin\ProductController@create');
         Route::post('/blogs/deleteimage', 'Admin\BlogController@deleteimage');
         Route::post('/blogs/deleteimage', 'Admin\BlogController@deleteimage');
    //     //Route::get('/categorydata', ['as' => 'CategoryControllerCategoryData', 'uses' => 'Admin\CategoryController@datatable']);

    //     Route::resource('/packages', 'Admin\PackageController');
    //     Route::get('/packages/create', 'Admin\PackageController@create');
    
        //Blog Module
        Route::resource('/blogs', 'Admin\BlogController');
        Route::get('/blogData', ['as' => 'blogData',
         'uses' => 'Admin\BlogController@datatable']);

       //  Route::get('/blogs/create', 'Admin\BlogController@create');

    //     Route::post('/packages/product', 'Admin\PackageController@productprice');
    //     Route::get('/opportunity', 'Admin\UsersController@opportunity');
    
        // Blog Category
        Route::resource('/blogcategory', 'Admin\BlogcategoryController');
        Route::get('/blogCategoryData', ['as' => 'blogCategoryData',
         'uses' => 'Admin\BlogcategoryController@datatable']);

        // Banner Category
        Route::resource('/bannerCategory', 'Admin\BannercategoryController');
        Route::get('/bannerCategoryData', ['as' => 'bannerCategoryData',
         'uses' => 'Admin\BannercategoryController@datatable']);
         Route::get('/portfoliocatsdata', ['as' => 'portfoliocatlistData', 'uses' => 'Admin\BannercategoryController@portfoliocatlistdatatable']);
        Route::get('/bannerCategory/{id}/portfoliocatlist', 'Admin\BannercategoryController@portfiliocatlistindex');
        
        // Banner 
        Route::resource('/banner', 'Admin\BannerController');
        Route::get('/bannerData', ['as' => 'bannerData',
         'uses' => 'Admin\BannerController@datatable']);

         // Free Banner 
        Route::resource('/freebanner', 'Admin\FreebannerController');
        Route::get('/freebannerData', ['as' => 'freebannerData','uses' => 'Admin\FreebannerController@datatable']);

         // Freeimage Userlog 
        Route::resource('/imageuserlog', 'Admin\FreeimagedownloaduserlogController');
        Route::get('/imageuserlogData', ['as' => 'imageuserlogData','uses' => 'Admin\FreeimagedownloaduserlogController@datatable']);

         // Portfolio 
        Route::resource('/portfolio', 'Admin\PortfolioController');
        Route::get('/portfolioData', ['as' => 'portfolioData',
         'uses' => 'Admin\PortfolioController@datatable']);
        
         // Bulk Upload 
        Route::get('/bulkupload', 'Admin\PortfolioController@bulkcreate');
        Route::post('/bulkupload', 'Admin\PortfolioController@bulkstore');

         // Setting
        Route::resource('/setting', 'Admin\SettingController');
        
        // contact us
        Route::resource('/contact', 'Admin\ContactController');
        Route::get('/contactData', ['as' => 'contactData',
         'uses' => 'Admin\ContactController@datatable']);
    });

});
Route::get('/home', 'HomeController@redirect');
Route::get('/capsuleowner', function()
{
	
   Artisan::call('CapsuleownerCommand:capsuleownerCommand');

    //
});

Route::get('/getopportunity', function()
{
	
   Artisan::call('Getopportunity:getopportunity');

    //
});




