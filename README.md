# My project's README

// Added Slug field on Banner Category Table
ALTER TABLE `bannercategory` ADD `slug` VARCHAR(191) NOT NULL AFTER `title`;

// Added Slug field on Blog Table
ALTER TABLE `blogs` ADD `slug` VARCHAR(191) NOT NULL AFTER `title`;

// Added Slug field on Blog Category Table
ALTER TABLE `blog_category` ADD `slug` VARCHAR(191) NOT NULL AFTER `title`;
