<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Portfolio extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'portfolio';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id','title','image','deleted_at','categoryId','featured_portfolio'];

    public function categoryName(){
        return $this->hasOne('App\Bannercategory','id','categoryId')->select('bannercategory.id as id','bannercategory.title as cat_title','bannercategory.slug as slug');
    }

   
}
