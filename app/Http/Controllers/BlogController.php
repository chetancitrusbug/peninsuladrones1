<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use App\BlogImages;
use App\Blogcategory;
use DB;

class BlogController extends Controller
{
     public function index()
    {
        $blogcategory = Blogcategory::with('blog')->where('status', '=', 'active')->get();
        $blogs = Blog::where('status', 'active')->latest()->paginate(6);
        return view('blog.index', compact('blogs','blogcategory'));
    }

    /**
     * 
     * @param type $slug
     * @return type
     */
    public function view($slug)
    {
        $blogcategory = Blogcategory::with('blog')->where('status', '=', 'active')->get();
        $blog = Blog::where('status', '=', 'active')->with('categoryName')->where('slug', '=', $slug)->firstOrFail();
       
        $recentBlogs = Blog::where('status', 'active')->where('category_id',$blog->category_id)->where('slug','!=',$slug)->limit(3)->latest()->get();
       
        $bloggallery = BlogImages::with('blog')->where('blog_id', '=', $blog->id)->get();

        return view('blog.view', compact('blog','bloggallery','recentBlogs','blogcategory'));
    }
    /**
     * 
     * @param type $category
     * @return type
     */
    public function serchbycategory($category)
    {
         $blogcategory = Blogcategory::where('status', '=', 'active')->where('slug','=',$category)->first();
         $blogs = Blog::where('status', 'active')->where('category_id','=',$blogcategory->id)->latest()->paginate(3);
        return view('blog.category', compact('blogs','blogcategory'));
    }


}
