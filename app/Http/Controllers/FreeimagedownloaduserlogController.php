<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FreeImageUserLog;
use App\Freebanner;
use Session;
use Response;

class FreeimagedownloaduserlogController extends Controller
{
	public function __construct()
    {
        $this->mail_function = new EmailController();
    }
    public function add(Request $request)
    {
      $freebanner = Freebanner::where('status', '=', 'active')->where('id','=',$request->input('image'))->first();     
		        $userlog = new FreeImageUserLog();
                $userlog->name = $request->input('name');
                $userlog->email = $request->input('email');
                $userlog->image = url('freebanner').'/'.$freebanner->image;
                $userlog->save();
				 $this->mail_function->sendMailuserImageAction($userlog);
       return json_encode(array('msg'=>'Success'));
       
       exit;
    }

    public function index()
    {   
        $freebanner = Freebanner::where('status', '=', 'active')->paginate(30);     
        return view('freedownload', compact('freebanner'));
    }
	
	public function checkvalid(Request $request)
    {   
	$user_id=base64_decode($request->get('id'));
	$message='';
	 $userlog = FreeImageUserLog::where('id',$user_id)->first();
	 if($userlog){
	 $start_date=strtotime($userlog->created_at);
	
	 $startsDate = time();
	 $expire_link_date= strtotime('+1 day', $start_date);
	  
	  if($expire_link_date < $startsDate){
		  $message='Sorry! Your Link is Expired';	
		  $userlog='';
	  }
	  else{
		  $userlog=basename($userlog->image);
		  $message='';
		  
		  $headers = array(
              'Content-Type: image/jpg',
            );
			return Response::download(public_path(). "/freebanner/".$userlog, $userlog, $headers);
			exit;
	  }
	  
		
        return view('checkvalidform', compact('message','userlog'));
    }
	else{
		  return redirect('/');
	}
	}
}
