<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use App\Blogcategory;
use DB;
use Session;
use App\BlogImages;
use Yajra\Datatables\Datatables;
use Image;
use File;
use App\Imagecounter;

class BlogController extends Controller
{
    public function convertVimeo($url)
    {

        if(preg_match(
                '/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/',
                $url,
                $matches
            ))
            {
        $id = $matches[2];  

        return 'http://player.vimeo.com/video/'.$id;
            }

    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request) {
        /*$keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $blogs = Blog::select('blogs.*')
            ->where('status', 'active')
            ->where('blogs.title', 'LIKE', "%$keyword%")
            ->orWhere('blogs.video_type', 'LIKE', "%$keyword%")
            ->paginate($perPage);
        } else {
            $blogs =  Blog::where('status','active')
           ->paginate($perPage);
        }                   
        return view('admin.blogs.index',compact('blogs'));*/
        return view('admin.blogs.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
       
       $blogs=Blog::with('categoryName');

       if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(blogs.title LIKE '%$value%' OR blogs.status LIKE '%$value%')";
                $blogs = Blog::with('categoryName')->whereRaw($where_filter);
            }
        }
        if ($request->get('status') != '') {
            $status = $request->get('status');
            $blogs = $blogs->where('blogs.status', $status);
            
        } 
           
        return Datatables::of($blogs)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {  
       $blogCategory = Blogcategory::where('status','active')->pluck('title','id')->prepend('Select Category',''); 
        return view('admin.blogs.create',compact('blogCategory'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        
		$this->validate($request, [
            'title' => 'required|unique:blogs',
            //'video_type' => 'required',
			 //'video_url' => 'required', 
            //'video_url' => ['required', 'regex:/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/'],
           // 'page_title' => 'required',
            'category_id' => 'required',
           // 'meta_keyword' => 'required',
           // 'og_title' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
           // 'mimage' => 'required',
            'mimage.*' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'shortdesc' => 'required',
            'longdescription' => 'required',
           // 'page_description' => 'required',
            //'og_description' => 'required',
        ]);

		$data = $request->all();
        if($request->video_type=='youtube'){
			$search     = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';
            if (preg_match($search, $request->video_url, $matches)) {
			$youtube_id = $matches[count($matches) - 1];
			}	
            $video_url = 'https://www.youtube.com/embed/' . $youtube_id ;
			$data['video_url']= $video_url;
        }else{
            $url = $request->video_url;
            $video_url = $this->convertVimeo($url);
            $data['video_url'] = $video_url;
   
        }
       
        $data['slug'] = str_slug($data['title']);
        $data['status'] = 'active';
        
        if ($request->hasFile('image')) {
            $image = $request->file('image');
           
            $img = Image::make($image->getRealPath());
            
            $destinationPath = public_path() . '/Blogs';
            File::exists($destinationPath) or File::makeDirectory($destinationPath);

            $path = $destinationPath . "/thumb";
            File::exists($path) or File::makeDirectory($path);
            $categoryName=Blogcategory::where('id','=',$request->input('category_id'))->first();
            if($request->input('title')){
                $img_slug = str_slug($request->input('title'));
            }else{
                $img_slug = str_slug($categoryName->title);
            }

            $checkimgcounter=Imagecounter::where('blog_cat_id',$request->input('category_id'))->first();
                if( $checkimgcounter){
                    $checkimgcounter->blog_img_counter+=1;
                    $checkimgcounter->save();
                    $imagename = $img_slug .'-'.$checkimgcounter->blog_img_counter. '.' . $image->getClientOriginalExtension();
                }else{
                        $imgcounter=new Imagecounter();
                        $imgcounter->blog_img_counter=1;
                        $imgcounter->blog_cat_id+=$request->input('category_id');
                        $imgcounter->save();
                        $imagename = $img_slug .'-'.$imgcounter->blog_img_counter. '.' . $image->getClientOriginalExtension();
                }


            // save original image
            $img->save($destinationPath . '/' . $imagename);

            // save thumbnail image
            $img->fit(480, 270, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/thumb/' . $imagename);
            $data['image'] = $imagename;
        }

        $blog = Blog::create($data);
         $files = $request->file('mimage');
        if ($request->hasFile('mimage')) {
            //$image = $request->file('image');
            $i=1;
            foreach ($files as $image) {
                $img = Image::make($image->getRealPath());
                
                $destinationPath = public_path() . '/Blogs';
                File::exists($destinationPath) or File::makeDirectory($destinationPath);

                $path = $destinationPath . "/thumb";
                File::exists($path) or File::makeDirectory($path);
                
               $categoryName=Blogcategory::where('id','=',$request->input('category_id'))->first();
                if($request->input('title')){
                    $img_slug = str_slug($request->input('title'));
                }else{
                    $img_slug = str_slug($categoryName->title);
                }
                
               $checkimgcounter=Imagecounter::where('blog_cat_id',$request->input('category_id'))->first();
                if( $checkimgcounter){
                    $checkimgcounter->blog_img_counter+=1;
                    $checkimgcounter->save();
                    $imagename = $img_slug .'-'.$checkimgcounter->blog_img_counter. '.' . $image->getClientOriginalExtension();
                }else{
                        $imgcounter=new Imagecounter();
                        $imgcounter->blog_img_counter=1;
                        $imgcounter->blog_cat_id+=$request->input('category_id');
                        $imgcounter->save();
                        $imagename = $img_slug .'-'.$imgcounter->blog_img_counter. '.' . $image->getClientOriginalExtension();
                }


                // save original image
                $img->save($destinationPath . '/' . $imagename);

                // save thumbnail image
                $img->fit(480, 270, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/thumb/' . $imagename);
                $dataimage['image']=$imagename;
                $dataimage['blog_id'] = $blog->id; 
                $image = BlogImages::create($dataimage);
               
            }
        }

        
        
       Session::flash('flash_message', 'Blog added!');
        return redirect('admin/blogs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {  
        $blog = Blog::where('id', $id)->first();
        
        if($blog == NULL) {
            Session::flash('flash_error', 'Blog is not exist!');
            return redirect('admin/blogs');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $blog->status= 'inactive';
                $blog->update();  
                return redirect()->back();
            }else{
                $blog->status= 'active';
                $blog->update();               
                return redirect()->back();
            }
        } 

        $blog['longdescription']=strip_tags($blog->longdescription);
        $blog['shortdesc']=strip_tags($blog->shortdesc);
        $blog['og_description']=strip_tags($blog->og_description);
        $blog['page_description']=strip_tags($blog->page_description);
        if($blog)
        {
            $images = BlogImages::where('blog_id',$id)->get();
            return view('admin.blogs.show', compact('blog','images'));
            
        }
        else{
            Session::flash('flash_error', 'Blog is not exist!');
            return redirect('admin/blogs');
        }
        
    }
    public function edit(Request $request,$id)
    {
        $blog = Blog::where('id',$id)->first();
        $blogCategory = Blogcategory::where('status','active')->pluck('title','id')->prepend('Select Category',''); 
        
        if($blog)
        {
            $images = BlogImages::where('blog_id',$id)->get();
            return view('admin.blogs.edit', compact('blog','images','blogCategory'));
        }
        else{
            Session::flash('flash_error', 'Blog is not exist!');
            return redirect('admin/blogs');
        }
    }
    
  
  
 /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
     
        $this->validate($request, [
            'title' => 'required',
          //  'video_type' => 'required',
           // 'video_url' => 'required', //'regex:/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/'],
           // 'page_title' => 'required',
            'status' => 'required',
            'category_id' => 'required',
           // 'meta_keyword' => 'required',
            //'og_title' => 'required',
            'image' => 'sometimes|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'mimage' => 'sometimes',
            'mimage.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'shortdesc' => 'required',
            'longdescription' => 'required',
           // 'page_description' => 'required',
            //'og_description' => 'required',
        ]);
        $requestData = $request->all(); 

        if($request->video_type =='youtube'){
             $search     = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';
            /*$replace    = "youtube.com/embed/$1";    
            $video_url = preg_replace($search,$replace,$request->video_url); */
			if (preg_match($search, $request->video_url, $matches)) {
			$youtube_id = $matches[count($matches) - 1];
			}	
            $video_url = 'https://www.youtube.com/embed/' . $youtube_id ;
			$requestData['video_url']= $video_url;

        }else if($request->video_type =='vimeo'){
             
            $url = $request->video_url;
            $video_url = $this->convertVimeo($url);
            if($video_url){
                 $requestData['video_url'] = $video_url;
            }
            else{
                $requestData['video_url'] = $request->video_url;
            }
            
   
        }else{
             $requestData['video_url'] = '';
        }
        
        $requestData['slug'] = str_slug($requestData['title']);    
        $Blog = Blog::findOrFail($id);
        if ($request->hasFile('image')) {
           
            $image = $request->file('image');
           
            $img = Image::make($image->getRealPath());
            
            $destinationPath = public_path() . '/Blogs';
            File::exists($destinationPath) or File::makeDirectory($destinationPath);

            $path = $destinationPath . "/thumb";
            File::exists($path) or File::makeDirectory($path);
            $categoryName=Blogcategory::where('id','=',$request->input('category_id'))->first();
            if($request->input('title')){
                $img_slug = str_slug($request->input('title'));
            }else{
                $img_slug = str_slug($categoryName->title);
            }

            $checkimgcounter=Imagecounter::where('blog_cat_id',$request->input('category_id'))->first();
                if( $checkimgcounter){
                    $checkimgcounter->blog_img_counter+=1;
                    $checkimgcounter->save();
                    $imagename = $img_slug .'-'.$checkimgcounter->blog_img_counter. '.' . $image->getClientOriginalExtension();
                }else{
                        $imgcounter=new Imagecounter();
                        $imgcounter->blog_img_counter=1;
                        $imgcounter->blog_cat_id+=$request->input('category_id');
                        $imgcounter->save();
                        $imagename = $img_slug .'-'.$imgcounter->blog_img_counter. '.' . $image->getClientOriginalExtension();
            }


            // save original image
            $img->save($destinationPath . '/' . $imagename);

            // save thumbnail image
            $img->fit(480, 270, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/thumb/' . $imagename);
           $requestData['image'] = $imagename;
        }


         $files = $request->file('mimage');
       
        if ($request->hasFile('mimage')) {
            if($Blog->image != '')
            {
                $urlArray = explode('/', $Blog->image);
                $imageName = end($urlArray);
                if(file_exists(public_path('Blogs').'/'.$imageName) AND !empty($imageName)){
                    unlink(public_path('Blogs').'/'.$imageName);
                }
            }
            //$image = $request->file('image');
            $i=1;
            foreach ($files as $image) {
                $img = Image::make($image->getRealPath());
                
                $destinationPath = public_path() . '/Blogs';
                File::exists($destinationPath) or File::makeDirectory($destinationPath);

                $path = $destinationPath . "/thumb";
                File::exists($path) or File::makeDirectory($path);
                
               $categoryName=Blogcategory::where('id','=',$request->input('category_id'))->first();
                if($request->input('title')){
                    $img_slug = str_slug($request->input('title'));
                }else{
                    $img_slug = str_slug($categoryName->title);
                }
                
                $checkimgcounter=Imagecounter::where('blog_cat_id',$request->input('category_id'))->first();
                if( $checkimgcounter){
                    $checkimgcounter->blog_img_counter+=1;
                    $checkimgcounter->save();
                    $imagename = $img_slug .'-'.$checkimgcounter->blog_img_counter. '.' . $image->getClientOriginalExtension();
                }else{
                        $imgcounter=new Imagecounter();
                        $imgcounter->blog_img_counter=1;
                        $imgcounter->blog_cat_id+=$request->input('category_id');
                        $imgcounter->save();
                        $imagename = $img_slug .'-'.$imgcounter->blog_img_counter. '.' . $image->getClientOriginalExtension();
                }


                // save original image
                $img->save($destinationPath . '/' . $imagename);

                // save thumbnail image
                $img->fit(480, 270, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/thumb/' . $imagename);
               $imageData['image']=$imagename;
                $imageData['blog_id'] = $id;
                 $image = BlogImages::create($imageData);
               
            }
        }
        
	    $Blog->update($requestData);
		Session::flash('flash_message', 'Blog Updated !');
		
        return redirect('admin/blogs');
    }

        
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
       
        $blog = Blog::find($id);
		if($blog->image){
        unlink(public_path('Blogs') . '/'.$blog->image);
		$images = BlogImages::where('blog_id',$id);
        $images->delete();
		}
        $blog->delete();
	
        

        if($request->ajax()){
            $message='Deleted';
             return response()->json(['message'=>$message],200);
        }else{
            Session::flash('flash_message','Blog Deleted Successfully!');            
            return redirect('admin/blogs');
        }
       
    }  


    public function deleteimage(Request $request)
    {
        $id =  $request->id;
        $image = BlogImages::where('id',$id)->first();
        $image->delete();
        $JSONARRAY = Array(
            'msg'=>'Success',
        );
        // echo json_encode($JSONARRAY);
        exit;
    }     

          

}
