<?php

namespace App\Http\Controllers\admin;

use App\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Yajra\Datatables\Datatables;
use Image;
use File;
use App\Imagecounter;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.banner.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        return view('admin.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
      
        $this->validate($request, [
            'title' => 'required|unique:banner',
            'short_description' => 'required',
            'link' => 'nullable|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'sort_no' => 'required|numeric',
        ]);
		
		$checksortno = Banner::where('sort_no',$request->input('sort_no'))->get();
		//dd($checksortno);
        $data = array(
            'title' => $request->input('title'),
            'short_description' => $request->input('short_description'),
            'link' => $request->input('link'),
            'status' => 'active',
			'sort_no'=>$request->input('sort_no'));
        $banner = Banner::create($data);
		
        if($checksortno){
			foreach($checksortno as $checksortnos){
				$checksortnos['sort_no']=0;
				$checksortnos->save();
			}
		} 
        if ($request->hasFile('image')) {
            $image = $request->file('image');
           
            $img = Image::make($image->getRealPath());
            
            $destinationPath = public_path() . '/banner';
            File::exists($destinationPath) or File::makeDirectory($destinationPath);

            $path = $destinationPath . "/thumb";
            File::exists($path) or File::makeDirectory($path);

             $imgName=$image->getClientOriginalName();
            $ext = pathinfo($imgName, PATHINFO_EXTENSION);
            $file = basename($imgName,".".$ext);
            if($request->input('title')){
                $img_slug = str_slug($request->input('title'));
            }else{
                $img_slug = str_slug($file);
            }
            
            $imgcounter=Imagecounter::first();
            $imgcounter->banner_counter+=1;
            $imgcounter->save();
            $imagename = $img_slug .'-'.$imgcounter->banner_counter. '.' . $image->getClientOriginalExtension();


            // save original image
            $img->save($destinationPath . '/' . $imagename);

            // save thumbnail image
            $img->fit(1920, 980, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/thumb/' . $imagename);
            $requestData['image'] = url('banner').'/'.$imagename;
            $banner = Banner::where('id', $banner->id);
            $banner->update($requestData);
        }   
        Session::flash('flash_message', 'Banner added!');
        return redirect('admin/banner');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $banner = Banner::where('id', $id)->first();

        if ($banner) {
            return view('admin.banner.edit', compact('banner'));
        } else {
            Session::flash('flash_error', 'Banner is not exist!');
            return redirect('admin/banner');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'short_description' => 'required',
            'link' => 'nullable|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'status' => 'required',
            'image' => 'sometimes|mimes:jpeg,png,jpg,gif,svg|max:2048',
             'sort_no' => 'required|numeric',
        ]);
       

        $banner = Banner::findOrFail($id);
        $requestData = array(
            'title' => $request->input('title'),
            'short_description' => $request->input('short_description'),
            'link' => $request->input('link'),
            'status' => $request->input('status'),
             'sort_no' => $request->input('sort_no'));
		$checksortno = Banner::where('sort_no',$request->input('sort_no'))->get();	 
		if($checksortno){
			foreach($checksortno as $checksortnos){
				$checksortnos['sort_no']=0;
				$checksortnos->save();
			}
		} 
		
        if ($request->file('image')) {
            if($banner->image != '')
            {
                $urlArray = explode('/', $banner->image);
                $imageName = end($urlArray);
                if(file_exists(public_path('banner').'/'.$imageName) AND !empty($imageName)){
                    unlink(public_path('banner').'/'.$imageName);
                }
            }
            $image = $request->file('image');
           
            $img = Image::make($image->getRealPath());
            
            $destinationPath = public_path() . '/banner';
            File::exists($destinationPath) or File::makeDirectory($destinationPath);

            $path = $destinationPath . "/thumb";
            File::exists($path) or File::makeDirectory($path);

            $imgName=$image->getClientOriginalName();
            $ext = pathinfo($imgName, PATHINFO_EXTENSION);
            $file = basename($imgName,".".$ext);
            if($request->input('title')){
                $img_slug = str_slug($request->input('title'));
            }else{
                $img_slug = str_slug($file);
            }
            $imgcounter=Imagecounter::first();
            $imgcounter->banner_counter+=1;
            $imgcounter->save();
            $imagename = $img_slug .'-'.$imgcounter->banner_counter. '.' . $image->getClientOriginalExtension();


            // save original image
            $img->save($destinationPath . '/' . $imagename);

            // save thumbnail image
            $img->fit(1920, 980, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/thumb/' . $imagename);
            $requestData['image'] = url('banner').'/'.$imagename;
            $banner->update($requestData);
        }  
           
        $banner = Banner::where('id', $id);
        $banner->update($requestData);
        Session::flash('flash_message', 'Banner Updated Successfully!');
        return redirect('admin/banner');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request, $id)
    {

        $banner = Banner::where('id', $id);
        $banner->delete();

        $message='Deleted';
        return response()->json(['message'=>$message],200);

    }
      /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $banner = Banner::All();
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(banner.title LIKE  '%$value%' OR banner.status LIKE  '%$value%'  )";
                $banner = Banner::whereRaw($where_filter);
            }
        }
        if ($request->get('status') != '') {
            $status = $request->get('status');
            $banner = $banner->where('status', $status);
            
        }
        return Datatables::of($banner)
            ->make(true);
        exit;
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $banner = Banner::where('id',$id)->first();
        $sort_no=$request->get('sort_no');
        $status =  $request->status;
        $checksortno = Banner::where('sort_no',$sort_no)->get();	 
		if($checksortno){
			foreach($checksortno as $checksortnos){
				$checksortnos['sort_no']=0;
				$checksortnos->save();
			}
		} 
        if($sort_no != null){

            $banner->sort_no = $sort_no;
            $banner->save();

            $message = "Sort No Changed Successfully !!";

            return response()->json(['messages' => $message],200);

        }
        if($banner != NULL)
        {
            //change client status
            $status = $request->get('status');
            if(!empty($status)){
                if($status == 'active' ){
                    $banner->status= 'inactive';
                    $banner->update();            

                    return redirect()->back();
                }else{
                    $banner->status= 'active';
                    $banner->update();               
                    return redirect()->back();
                }

            }
            return view('admin.banner.show', compact('banner'));
        }
        else {
            Session::flash('flash_error', 'Banner is not exist!');
            return redirect('admin/banner');
        }
    }
}
