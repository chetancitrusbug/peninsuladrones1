<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Freebanner;
use File;
use Image;
use Session;
use Yajra\Datatables\Datatables;
use App\Imagecounter;

class FreebannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.freebanner.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        return view('admin.freebanner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
           
            $img = Image::make($image->getRealPath());
            
            $destinationPath = public_path() . '/freebanner';
            File::exists($destinationPath) or File::makeDirectory($destinationPath);

            $path = $destinationPath . "/thumb";
            File::exists($path) or File::makeDirectory($path);

            $imgName=$image->getClientOriginalName();
            $ext = pathinfo($imgName, PATHINFO_EXTENSION);
            $file = basename($imgName,".".$ext);
            $img_slug = str_slug($file);
            
            $imgcounter=Imagecounter::first();
            $imgcounter->freebanner_counter+=1;
            $imgcounter->save();
            $imagename = $img_slug .'-'.$imgcounter->freebanner_counter. '.' . $image->getClientOriginalExtension();


            // save original image
            $img->save($destinationPath . '/' . $imagename);

            // save thumbnail image
            $img->fit(480, 270, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/thumb/' . $imagename);
            $freebanner = new Freebanner();
            $freebanner['title'] = $request->input('title');
            $freebanner['image'] = $imagename;
            $freebanner->save();
        }   
        Session::flash('flash_message', 'Banner added!');
        return redirect('admin/freebanner');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $freebanner = Freebanner::where('id', $id)->first();

        if ($freebanner) {
            return view('admin.freebanner.edit', compact('freebanner'));
        } else {
            Session::flash('flash_error', 'Banner is not exist!');
            return redirect('admin/freebanner');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'image' => 'sometimes|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
       
        $freebanner = Freebanner::findOrFail($id);
        $freebanner['title'] = $request->input('title');
        $freebanner->save();
        if ($request->hasFile('image')) {
            if($freebanner->image != '')
            {
                if(file_exists(public_path('freebanner').'/'.$freebanner->image) AND !empty($freebanner->image)){
                    unlink(public_path('freebanner').'/'.$freebanner->image);
                    unlink(public_path('freebanner/thumb').'/'.$freebanner->image);
                }
            }
            $image = $request->file('image');
           
            $img = Image::make($image->getRealPath());
            
            $destinationPath = public_path() . '/freebanner';
            File::exists($destinationPath) or File::makeDirectory($destinationPath);

            $path = $destinationPath . "/thumb";
            File::exists($path) or File::makeDirectory($path);


            $imgName=$image->getClientOriginalName();
            $ext = pathinfo($imgName, PATHINFO_EXTENSION);
            $title = $request->input('title');
            $file = basename($imgName,".".$ext);
            if($request->input('title')){
             $img_slug = str_slug($request->input('title'));
            }else{
                $img_slug = str_slug($file);
            }
            $imgcounter=Imagecounter::first();
            $imgcounter->freebanner_counter+=1;
            $imgcounter->save();
            $imagename = $img_slug .'-'.$imgcounter->freebanner_counter. '.' . $image->getClientOriginalExtension();


            // save original image
            $img->save($destinationPath . '/' . $imagename);

            // save thumbnail image
            $img->fit(480, 270, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/thumb/' . $imagename);
          
            $freebanner['image'] = $imagename;
            $freebanner->save();
        }   

        Session::flash('flash_message', 'Banner Updated Successfully!');
        return redirect('admin/freebanner');
    }

     /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $freebanner = Freebanner::orderby('id','desc')->select('*');
        //dd($freebanner);
        return Datatables::of($freebanner)
            ->make(true);
        exit;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $freebanner = Freebanner::where('id',$id)->first();
        
        if($freebanner != NULL)
        {
            //change client status
            $status = $request->get('status');
            if(!empty($status)){
                if($status == 'active' ){
                    $freebanner->status= 'inactive';
                    $freebanner->update();            

                    return redirect()->back();
                }else{
                    $freebanner->status= 'active';
                    $freebanner->update();               
                    return redirect()->back();
                }

            }
            return view('admin.freebanner.show', compact('freebanner'));
        }
        else {
            Session::flash('flash_error', 'Banner is not exist!');
            return redirect('admin/freebanner');
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request, $id)
    {

        $freebanner = Freebanner::where('id', $id)->first();
        if($freebanner->image){
        unlink(public_path('freebanner') . '/'.$freebanner->image);
        unlink(public_path('freebanner/thumb/') . '/'.$freebanner->image);
        }
         $freebanner->delete();

        $message='Deleted';
        return response()->json(['message'=>$message],200);

    }

}
