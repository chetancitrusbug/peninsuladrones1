<?php

namespace App\Http\Controllers\Admin;

use App\Bannercategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Yajra\Datatables\Datatables;
use App\Portfolio;

class BannercategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.bannerCategory.index');
        /* $bannerCategory = Bannercategory::orderBy('id', 'DESC');
        $bannerCategory = $bannerCategory->get();
        return view('admin.bannerCategory.index', compact('bannerCategory'));*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        return view('admin.bannerCategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:bannercategory'
        ]);
        $data = array(
            'title' => $request->input('title'),
            'status' => 'active');

        $data['slug'] = str_slug($data['title']); 
        $bannerCategory = Bannercategory::create($data);

        Session::flash('flash_message', 'Portfolio added!');
        return redirect('admin/bannerCategory');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $bannerCategory = Bannercategory::where('id', $id)->first();

        if ($bannerCategory) {
            return view('admin.bannerCategory.edit', compact('bannerCategory'));
        } else {
            Session::flash('flash_error', 'Portfolio is not exist!');
            return redirect('admin/bannerCategory');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'status' => 'required',
        ]);       
        
        $requestData = array(
            'title' => $request->input('title'),
            'status' => $request->input('status'));

        $requestData['slug'] = str_slug($requestData['title']); 
        $bannerCategory = Bannercategory::where('id', $id);
        $bannerCategory->update($requestData);
        Session::flash('flash_message', 'Portfolio Updated Successfully!');
        return redirect('admin/bannerCategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request, $id)
    {

        $bannerCategory = Bannercategory::where('id', $id);
         $portfolio = Portfolio::where('categoryId', $id)->get();
         foreach($portfolio as $portfolios){
                 $portfolios->delete();
         }
     
        $bannerCategory->delete();

        $message='Deleted';
        return response()->json(['message'=>$message],200);

    }
      /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $bannerCategory = Bannercategory::All();
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(bannercategory.title LIKE  '%$value%' OR bannercategory.status LIKE  '%$value%'  )";
                $bannerCategory = Bannercategory::whereRaw($where_filter);
            }
        }
        if ($request->get('status') != '') {
            $status = $request->get('status');
            $bannerCategory = $bannerCategory->where('status', $status);
            
        }
        return Datatables::of($bannerCategory)
            ->make(true);
        exit;
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $bannerCategory = Bannercategory::where('id', $id)->first();
        if($bannerCategory == NULL) {
            Session::flash('flash_error', 'Portfolio is not exist!');
            return redirect('admin/bannerCategory');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $bannerCategory->status= 'inactive';
                $bannerCategory->update();            

                return redirect()->back();
            }else{
                $bannerCategory->status= 'active';
                $bannerCategory->update();               
                return redirect()->back();
            }

        }
        return view('admin.bannerCategory.show', compact('bannerCategory'));
    }
    public function portfiliocatlistindex(request $request,$id)
    {
       $cat_id=$id;
       //dd( $cat_id);
        return view('admin.bannerCategory.portfoliocat',compact('cat_id'));
    }

    public function portfoliocatlistdatatable(request $request)
    {
      //  dd($request->get('cat_id'));
    $portfolio=Portfolio::with('categoryName')->where('portfolio.categoryId','=',$request->get('cat_id'))->get();

       if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(portfolio.title LIKE  '%$value%' OR portfolio.status LIKE  '%$value%'  )";
                $portfolio = Portfolio::with('categoryName')->where('portfolio.categoryId','=',$request->get('cat_id'))->whereRaw($where_filter);
            }
        }
        // if ($request->get('status') != '') {
        //     $status = $request->get('status');
        //     $portfolio = $portfolio->where('portfolio.categoryId','=',$request->get('cat_id'))->where('portfolio.status', $status);
            
        // }        
        return Datatables::of($portfolio)
            ->make(true);
        exit;
    }
}
