<?php

namespace App\Http\Controllers\admin;

use App\Portfolio;
use App\Bannercategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Yajra\Datatables\Datatables;
use Image;
use File;
use App\Imagecounter;

class PortfolioController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.portfolio.index');
    }
    public function bulkindex(Request $request)
    {
        return view('admin.bulkupload.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        $categoryId = $request->get('cat');
        $bannerCategory = Bannercategory::where('status','active')->pluck('title','id')->prepend('Select Category',''); 
		return view('admin.portfolio.create',compact('bannerCategory','categoryId'));
    }

    public function bulkcreate(Request $request)
    {
        $categoryId = $request->get('cat');
        $bannerCategory = Bannercategory::where('status','active')->pluck('title','id')->prepend('Select Category',''); 
		return view('admin.bulkupload.create',compact('bannerCategory','categoryId'));
    }

     public function bulkstore(Request $request)
    {
        $this->validate($request, [
            'image' => 'required'
        ]);
        $files = $request->file('image');
        if ($request->hasFile('image')) {
            //$image = $request->file('image');
            $i=1;
            foreach ($files as $image) {
                $img = Image::make($image->getRealPath());
                
                $destinationPath = public_path() . '/portfolio';
                File::exists($destinationPath) or File::makeDirectory($destinationPath);

                $path = $destinationPath . "/thumb";
                File::exists($path) or File::makeDirectory($path);
                
                $categoryName=Bannercategory::where('id','=',$request->input('categoryId'))->first();
                if($request->input('title')){
                    $img_slug = str_slug($request->input('title'));
                }else{
                    $img_slug = str_slug($categoryName->title);
                }
            
                $checkimgcounter=Imagecounter::where('portfolio_cat_id',$request->input('categoryId'))->first();
                if( $checkimgcounter){
                    $checkimgcounter->img_counter+=1;
                    $checkimgcounter->save();
                    $imagename = $img_slug .'-'.$checkimgcounter->img_counter. '.' . $image->getClientOriginalExtension();
                }else{
                        $imgcounter=new Imagecounter();
                        $imgcounter->img_counter=1;
                        $imgcounter->portfolio_cat_id+=$request->input('categoryId');
                        $imgcounter->save();
                        $imagename = $img_slug .'-'.$imgcounter->img_counter. '.' . $image->getClientOriginalExtension();
                }


                // save original image
                $img->save($destinationPath . '/' . $imagename);

                // save thumbnail image
                $img->fit(480, 270, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/thumb/' . $imagename);
                 $portfolio = new Portfolio();
                $portfolio['image'] = url('portfolio').'/'.$imagename;
                $portfolio->categoryId=$request->input('categoryId');
               // dd($portfolio->categoryId);
                $portfolio->save();
               
            }
        }

        Session::flash('flash_message', 'Portfolio added!');
        return redirect('admin/bannerCategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $data = array(
            'title' => $request->input('title'),
            'categoryId' => $request->input('categoryId'),
            'status' => 'active',
            'featured_portfolio' => $request->input('featured_portfolio'));
        $portfolio = Portfolio::create($data);
         if ($request->hasFile('image')) {
            $image = $request->file('image');
           
            $img = Image::make($image->getRealPath());
            
            $destinationPath = public_path() . '/portfolio';
            File::exists($destinationPath) or File::makeDirectory($destinationPath);

            $path = $destinationPath . "/thumb";
            File::exists($path) or File::makeDirectory($path);
            $categoryName=Bannercategory::where('id','=',$request->input('categoryId'))->first();
            if($request->input('title')){
                $img_slug = str_slug($request->input('title'));
            }else{
                $img_slug = str_slug($categoryName->title);
            }
            
            $checkimgcounter=Imagecounter::where('portfolio_cat_id',$request->input('categoryId'))->first();
            if( $checkimgcounter){
                $checkimgcounter->img_counter+=1;
                $checkimgcounter->save();
                 $imagename = $img_slug .'-'.$checkimgcounter->img_counter. '.' . $image->getClientOriginalExtension();
            }else{
                    $imgcounter=new Imagecounter();
                    $imgcounter->img_counter=1;
                    $imgcounter->portfolio_cat_id+=$request->input('categoryId');
                    $imgcounter->save();
                    $imagename = $img_slug .'-'.$imgcounter->img_counter. '.' . $image->getClientOriginalExtension();
            }

            // save original image
            $img->save($destinationPath . '/' . $imagename);

            // save thumbnail image
            $img->fit(480, 270, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/thumb/' . $imagename);
            $portfolio['image'] = url('portfolio').'/'.$imagename;
            $portfolio->save();
        } 
        
        Session::flash('flash_message', 'Portfolio added!');
        return redirect('admin/bannerCategory');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $portfolio = Portfolio::where('id', $id)->with('categoryName')->first();
        $bannerCategory = Bannercategory::where('status','active')->pluck('title','id')->prepend('Select Category',''); 
        if ($portfolio) {
            return view('admin.portfolio.edit', compact('portfolio','bannerCategory'));
        } else {
            Session::flash('flash_error', 'Portfolio is not exist!');
            return redirect('admin/bannerCategory');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'status' => 'required',
            'image' => 'sometimes|mimes:jpeg,png,jpg,gif,svg',
        ]);
       

        $portfolio = Portfolio::findOrFail($id);
        $requestData = array(
            'title' => $request->input('title'),
            'status' => $request->input('status'),
             'featured_portfolio' => $request->input('featured_portfolio'));
        if ($request->file('image')) {
            
            if($portfolio->image != '')
            {
                $urlArray = explode('/', $portfolio->image);
                $imageName = end($urlArray);
                if(file_exists(public_path('portfolio').'/'.$imageName) AND !empty($imageName)){
                    unlink(public_path('portfolio').'/'.$imageName);
                }
            }
            $image = $request->file('image');
           
            $img = Image::make($image->getRealPath());
            
            $destinationPath = public_path() . '/portfolio';
            File::exists($destinationPath) or File::makeDirectory($destinationPath);

            $path = $destinationPath . "/thumb";
            File::exists($path) or File::makeDirectory($path);

            $categoryName=Bannercategory::where('id','=',$portfolio->categoryId)->first();
            if($request->input('title')){
                $img_slug = str_slug($request->input('title'));
            }else{
                $img_slug = str_slug($categoryName->title);
            }
            
            $checkimgcounter=Imagecounter::where('portfolio_cat_id',$portfolio->categoryId)->first();
            if( $checkimgcounter){
                $checkimgcounter->img_counter+=1;
                $checkimgcounter->save();
                 $imagename = $img_slug .'-'.$checkimgcounter->img_counter. '.' . $image->getClientOriginalExtension();
            }else{
                    $imgcounter=new Imagecounter();
                    $imgcounter->img_counter=1;
                    $imgcounter->portfolio_cat_id+=$portfolio->categoryId;
                    $imgcounter->save();
                    $imagename = $img_slug .'-'.$imgcounter->img_counter. '.' . $image->getClientOriginalExtension();
            }


            // save original image
            $img->save($destinationPath . '/' . $imagename);

            // save thumbnail image
            $img->fit(480, 270, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/thumb/' . $imagename);
            $portfolio['image'] = url('portfolio').'/'.$imagename;
            $portfolio->save();
            
        }
        $portfolio = portfolio::where('id', $id);
        $portfolio->update($requestData);
        Session::flash('flash_message', 'Portfolio Updated Successfully!');
        return redirect('admin/bannerCategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request, $id)
    {

        $portfolio = Portfolio::where('id', $id);
        $portfolio->delete();

        $message='Deleted';
        return response()->json(['message'=>$message],200);

    }
      /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {       
       $portfolio=Portfolio::with('categoryName');

       if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(portfolio.title LIKE  '%$value%' OR portfolio.status LIKE  '%$value%'  )";
                $portfolio = Portfolio::with('categoryName')->whereRaw($where_filter);
            }
        }
        if ($request->get('status') != '') {
            $status = $request->get('status');
            $portfolio = $portfolio->where('portfolio.status', $status);
            
        }        
        return Datatables::of($portfolio)
            ->make(true);
        exit;
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $portfolio = Portfolio::with('categoryName')->where('id', $id)->first();        
        if($portfolio == NULL) {
            Session::flash('flash_error', 'Portfolio is not exist!');
            return redirect('admin/portfolio');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $portfolio->status= 'inactive';
                $portfolio->update();  
                return redirect()->back();
            }else{
                $portfolio->status= 'active';
                $portfolio->update();               
                return redirect()->back();
            }
        }
        return view('admin.portfolio.show', compact('portfolio'));
    }

    
}
