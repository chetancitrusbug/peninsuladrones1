<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Portfolio;
use App\Bannercategory;

class PortfolioController extends Controller
{
    /**
     * 
     * @param type $title
     * @return type
     */
    public function view($slug)
    {   
        $portfoliocategory=Bannercategory::where('slug',$slug)->where('status','active')->first();
        //dd($portfoliocategory);
        if(count($portfoliocategory)>0){
             $portfolio = Portfolio::where('status', '=', 'active')->where('categoryId','=',$portfoliocategory->id)->paginate(30);
            return view('portfolio.view', compact('portfolio','portfoliocategory'));      
        }
        else{
            flash('Category Does not Exist');		
             return redirect('/');
        }
        
       
    }
}
