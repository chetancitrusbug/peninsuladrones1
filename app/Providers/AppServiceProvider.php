<?php

namespace App\Providers;

use App\WebSite;
use Illuminate\Support\ServiceProvider;
use Schema;
use App\Bannercategory;
use App\Setting;

class AppServiceProvider extends ServiceProvider
{

    protected static $_websites = null;
    protected static $_portfolio_category = null;
    protected static $_websites_pluck = null;
     protected static $_setting = null;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        
        if (is_null(self::$_websites) && \Schema::hasTable('_websites')) {

            self::$_websites = WebSite::active()->get();
            self::$_websites_pluck = WebSite::select(
                \DB::raw("CONCAT(name,' | http://',domain) AS works"), 'domain', 'id')
                ->active()->pluck('domain', 'id')->prepend('Select..', '');
        }
         self::$_portfolio_category = Bannercategory::where('status','active')->get();
         self::$_setting = Setting::where('id','1')->get();
        // Using view composer to set following variables globally
        view()->composer('*', function ($view) {
            $view->with('_websites', self::$_websites);
            $view->with('_websites_pluck', self::$_websites_pluck);
            $view->with('_portfolio_category', self::$_portfolio_category);
            $view->with('_setting', self::$_setting);

        });
     
       
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
