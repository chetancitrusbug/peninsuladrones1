<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bannercategory extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bannercategory';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id','title','slug','status','deleted_at'];

    public function getportfolio(){
        return $this->hasMany('App\Portfolio','categoryId','id')->where('featured_portfolio','1')->where('status','active')->with('CategoryName')->orderBy('created_at','desc');
    }
}

