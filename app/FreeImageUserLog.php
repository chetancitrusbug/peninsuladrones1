<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreeImageUserLog extends Model
{
    public $table='freedownloadimage_user_log';
       /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name','email','image'];
}
