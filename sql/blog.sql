/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.1.21-MariaDB : Database - bloglaravel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;




/*Table structure for table `blogs` */

DROP TABLE IF EXISTS `blogs`;

CREATE TABLE `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shortdesc` longtext COLLATE utf8mb4_unicode_ci,
  `longdescription` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci,
  `meta_keyword` longtext COLLATE utf8mb4_unicode_ci,
  `og_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `og_description` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `blogs` */

insert  into `blogs`(`id`,`title`,`shortdesc`,`longdescription`,`image`,`video_type`,`video_url`,`meta_title`,`meta_description`,`meta_keyword`,`og_title`,`og_description`,`status`,`deleted_at`,`created_at`,`updated_at`) values (1,'dfds','dfsdf','sdfsd',NULL,'youtube','dfsd','dfsdf','sdfsd','fsdfsd','fsdfsdf','sdfsdf','active',NULL,'2018-02-28 14:49:26','2018-02-28 14:49:26'),(2,'sdsd','dffdsfdsf','dsadsds','15198811935a978be9eb465.png','youtube','sdsads','sdsd','dsdsd','sdsds','sdsd','dsdsds','active',NULL,'2018-03-01 05:13:13','2018-03-01 05:13:13');

/*Table structure for table `capsule_owner` */

DROP TABLE IF EXISTS `capsule_owner`;

CREATE TABLE `capsule_owner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `capsule_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `capsule_owner` */

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'active',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `category` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_193651_create_roles_permissions_tables',1),(4,'2018_02_26_070656_create_category_table',1),(5,'2018_02_26_083346_create_product_table',1),(6,'2018_02_26_101056_create_package_table',1),(7,'2018_02_27_091654_create_capsule_owner_table',1),(8,'2018_02_27_094920_add_field_to_users',1),(9,'2018_02_27_101610_add_field_to_package',1),(10,'2018_02_28_073418_opportunity_table',1),(12,'2018_02_28_135001_create_blog_table',2);

/*Table structure for table `opportunity` */

DROP TABLE IF EXISTS `opportunity`;

CREATE TABLE `opportunity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `opp_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `opp_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `closed` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `opportunity` */

/*Table structure for table `package_product` */

DROP TABLE IF EXISTS `package_product`;

CREATE TABLE `package_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `package_product` */

/*Table structure for table `packages` */

DROP TABLE IF EXISTS `packages`;

CREATE TABLE `packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `kw` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `packages` */

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permission_role` */

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_role` */

insert  into `permission_role`(`permission_id`,`role_id`) values (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1);

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`parent_id`,`name`,`label`,`deleted_at`,`created_at`,`updated_at`) values (1,0,'access.users','Can Access Users',NULL,'2018-02-28 13:46:17','2018-02-28 13:46:17'),(2,1,'access.user.create','Can Create User',NULL,'2018-02-28 13:46:17','2018-02-28 13:46:17'),(3,1,'access.user.edit','Can Edit User',NULL,'2018-02-28 13:46:17','2018-02-28 13:46:17'),(4,1,'access.user.delete','Can Delete User',NULL,'2018-02-28 13:46:17','2018-02-28 13:46:17'),(5,0,'access.roles','Can Access Roles',NULL,'2018-02-28 13:46:17','2018-02-28 13:46:17'),(6,5,'access.role.create','Can Create Role',NULL,'2018-02-28 13:46:17','2018-02-28 13:46:17'),(7,5,'access.role.edit','Can Edit Role',NULL,'2018-02-28 13:46:17','2018-02-28 13:46:17'),(8,5,'access.role.delete','Can Delete Role',NULL,'2018-02-28 13:46:17','2018-02-28 13:46:17'),(9,0,'access.permissions','Can Access Permission',NULL,'2018-02-28 13:46:17','2018-02-28 13:46:17'),(10,9,'access.permission.create','Can Create Permission',NULL,'2018-02-28 13:46:17','2018-02-28 13:46:17'),(11,9,'access.permission.edit','Can Edit Permission',NULL,'2018-02-28 13:46:17','2018-02-28 13:46:17'),(12,9,'access.permission.delete','Can Delete Permission',NULL,'2018-02-28 13:46:17','2018-02-28 13:46:17'),(13,0,'access.services','Can Access Services',NULL,'2018-02-28 13:46:17','2018-02-28 13:46:17'),(14,13,'access.service.create','Can Create Service',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(15,13,'access.service.edit','Can Edit Service',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(16,13,'access.service.delete','Can Delete Service',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(17,0,'access.qrcode','Can Access Qrcode',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(18,17,'access.qrcode.create','Can Create Qrcode',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(19,17,'access.qrcode.edit','Can Edit Qrcode',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(20,17,'access.qrcode.delete','Can Delete Qrcode',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(21,0,'access.api','Can Access Api',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(22,0,'access.package','Can Access Package',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(23,22,'access.packages.create','Can Create Package',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(24,22,'access.packages.edit','Can Edit Package',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(25,22,'access.packages.delete','Can Delete Package',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(26,0,'access.order','Can Access Order',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(27,26,'access.orders.create','Can Create Order',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(28,26,'access.orders.edit','Can Edit Order',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18'),(29,26,'access.orders.delete','Can Delete Order',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18');

/*Table structure for table `product_image` */

DROP TABLE IF EXISTS `product_image`;

CREATE TABLE `product_image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `product_image` */

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shortdesc` longtext COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `sale_price` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `products` */

/*Table structure for table `role_user` */

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_user` */

insert  into `role_user`(`role_id`,`user_id`) values (1,1);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`label`,`deleted_at`,`created_at`,`updated_at`) values (1,'SU','Super User',NULL,'2018-02-28 13:46:18','2018-02-28 13:46:18');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_website_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'en',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT '0',
  `updated_by` int(11) DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `capsule_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`_website_id`,`name`,`email`,`password`,`language`,`deleted_at`,`created_by`,`updated_by`,`remember_token`,`created_at`,`updated_at`,`capsule_id`) values (1,1,'Admin','admin.citrusbug@gmail.com','$2y$10$uo98Fa3yZ5urVEGq4SJogOxrMHmnm1EVY0jpAwon2UM6PMGxwkhyu','en',NULL,0,0,NULL,'2018-02-28 13:46:20','2018-02-28 13:46:20',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
